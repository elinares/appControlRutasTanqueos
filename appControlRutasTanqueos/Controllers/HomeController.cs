﻿
using appControlRutasTanqueos.Areas.Privada.Models;
using appControlRutasTanqueos.Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace appControlRutasTanqueos.Controllers
{
    public class HomeController : Controller
    {
        public interfaz objInt = new interfaz();

        public ActionResult Index()
        {
            ViewBag.Titulo = objInt.titulo;
            ViewBag.Title = objInt.title;
            ViewBag.Subtitulo = "INICIO";
            return View();

        }

        public ActionResult ControlRutasTanqueos()
        {

            ViewBag.Titulo = objInt.titulo;
            ViewBag.Title = objInt.title;
            ViewBag.Subtitulo = "Control de kilometraje y consumo por ruta recorrida";
            return View();
            //return View("ControlRutasTanqueos"); //Otra forma

        }




        public ActionResult RpteGlnsAsigVSGlnsAbast()
        {

            ViewBag.Titulo = objInt.titulo;
            ViewBag.Title = objInt.title;
            ViewBag.Subtitulo = "Reporte de galones asignados VS galones abastecidos";
            return View();

        }
        public ActionResult RpteDsctConductores()
        {

            ViewBag.Titulo = objInt.titulo;
            ViewBag.Title = objInt.title;
            ViewBag.Subtitulo = "Listado de descuento a conductores por consumo excesivo de combustible";
            return View();

        }


        
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(appControlRutasTanqueos.Startup))]
namespace appControlRutasTanqueos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

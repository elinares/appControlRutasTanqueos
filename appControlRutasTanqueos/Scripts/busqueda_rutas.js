﻿
var table_Rut = "tinytable_Rut", popup_Rut = "xf1";

$(document).ready(function () {

    agregarEventosTabla_Rut();

});

//-------------------Uso de JSON para conectarse a la BD por JavaScript------------

var ruta = function (id_ruta, nombre,peso) {
    this.id_ruta = id_ruta;
    this.nombre = nombre;
    this.tiempo_aprox = 0.0;
    this.km_aprox = 0.0;    
    this.rend_esperado = 0.0;
    this.peso = peso;
}
function getFormJson(i) {
    var obj = new ruta(i, $('#campo_filtrar_Rut').val(),$('#campo4_rutas').val());
    return obj;
}


function obtenerDatos_Rut(i, event) {
    //
    var obj = getFormJson(i);//$(this).serializeObject(); //
    var nuevaFila = "";
    //alert("Hola " + obj.nombre);
    if ($('#campo_filtrar_Rut').val().trim() != "") {
        
        if (event.which != 13 && event.which != 38 && event.which != 40) {
            quitarTodaFilas_Rut();
            
            $.ajax({
                url: '/Ruta/ajax_listaRutas/',
                type: 'POST',
                contentType: 'application/json;charset=utf-8',
                data: JSON.stringify(obj),
                success: function (lista) {

                    $.each(lista, function (key, p) {

                        var cantfilas = $('#' + table_Rut + ' >tbody >tr').length;
                        nuevaFila = "<tr>";
                        nuevaFila += "<td>" + p.id_ruta + "</td>";
                        nuevaFila += "<td>" + p.nombre + "</td>";
                        nuevaFila += "<td>" + p.km_aprox + "</td>";
                        nuevaFila += "<td>" + p.tiempo_aprox + "</td>";
                        nuevaFila += "<td>" + p.rend_esperado + "</td>";
                        
                        nuevaFila += "</tr>";

                        $("#" + table_Rut).append(nuevaFila);

                    });

                    agregarEventosTabla_Rut();
                    $("#" + table_Rut + " tbody tr:first").find("td").addClass('hover');

                },
                error: function (e) {
                    alert('Error: ' + e);
                }
            });

        }
    }
    else {
        $('#campo_filtrar_Rut').select();
        $('#campo_filtrar_Rut').focus();
    }

    if (event.which == 38 || event.which == 40) moverseFilasTabla_Rut(event);
    if (event.which == 13) mostrarDatoSeleccionado_Rut();
    return false; //Esto anula el evento Submit()

}


function moverseFilasTabla_Rut(event) {

    var sw = 0;
    $('#' + table_Rut + ' tbody tr').each(function (key) {

        //alert($(this).find('td').eq(0).hasClass("hover"));
        if ($(this).find('td').eq(0).hasClass("hover")) {
            controlaSeleccion_Rut(key, event.which);
            sw = 1;
            return false;
        }

    });
    if (sw == 0) $("#" + table_Rut + " tbody tr:first").find("td").addClass('hover');
    return true;

}

function mostrarDatoSeleccionado_Rut() {

    $('#' + table_Rut + ' tbody tr').each(function (key) {

        //apopup_Rutlert("Hola");
        if ($(this).find('td').eq(0).hasClass("hover")) {
            mostrarDatos_Rut($(this).find("td").eq(0).html(), $(this).find("td").eq(1).html(), $(this).find("td").eq(2).html(), $(this).find("td").eq(4).html());
            ocultaPopup(popup_Rut);
            //xFenster.hideAll();
            //xFenster.instances[popup_Rut].hide();
            //desactivarSombraPantalla();
            quitarTodaFilas_Rut();
            return false;
        }

    });
    return true;

}

function ocultaPopup(popup) {
    //alert("Hola");
    xFenster.instances[popup].hide();
    desactivarSombraPantalla();
}

function quitarTodaFilas_Rut() {

    //alert("Holaaaa");
    var trs = $("#" + table_Rut + " tbody tr").length;
    for (var i = 0; i < trs; i++) {
        $("#" + table_Rut + " tbody tr").remove();
    }
}

function agregarEventosTabla_Rut() {

    $('#' + table_Rut + ' tbody tr').hover(

      function () {
          $('#' + table_Rut).find('tbody tr td').removeClass('hover');//Deseleccionamos todo
          $(this).find('td').addClass('hover');
      },
      function () {
          $(this).find('td').removeClass('hover');

      }

  );

    $('#' + table_Rut + ' tbody tr').click(function () {

        $('#' + table_Rut).find('tbody tr td').removeClass('hover');//Deseleccionamos todo
        $(this).find('td').addClass('hover');
        mostrarDatos_Rut($(this).find("td").eq(0).html(), $(this).find("td").eq(1).html(), $(this).find("td").eq(2).html(), $(this).find("td").eq(4).html());
        ocultaPopup(popup_Rut);
        quitarTodaFilas_Rut();

    });

}

function controlaSeleccion_Rut(pos, event) {
    
    $('#' + table_Rut + ' tbody tr').each(function (key) {

        //alert(key+""+event);
        if (key == pos - 1 && event == 38)//Tecla Hacia arriba
        {
            $('#' + table_Rut).find('tbody tr td').removeClass('hover');//Deseleccionamos todo
            $(this).find('td').addClass('hover');//Seleccionamos el anterior
            $("#campo_filtrar_Rut").val($(this).find("td").eq(1).html());
            $("#campo_filtrar_Rut").select();
            return false; 
        }
        else {
            if (key == pos + 1 && event == 40)//Tecla Hacia abajo
            {
                $('#' + table_Rut).find('tbody tr td').removeClass('hover');//Deseleccionamos todo
                $(this).find('td').addClass('hover');//Seleccionamos el siguiente
                $("#campo_filtrar_Rut").val($(this).find("td").eq(1).html());
                $("#campo_filtrar_Rut").select();
                return false;
            }

        }

    });

}

function mostrarDatos_Rut(id_ruta, nombre, km_aprox, rend_esperado) {

    //alert($('#'+ $('#campo2_Rutcepto').val()).val());
    //alert(id_ruta+ "-"+ nombre)
    $('#' + $('#campo0_rutas').val()).val(id_ruta);
    $('#' + $('#campo1_rutas').val()).val(nombre);
    $('#' + $('#campo1_rutas').val()).focus();
    $('#' + $('#campo1_rutas').val()).select();
    $('#' + $('#campo2_rutas').val()).val(km_aprox);
    $('#' + $('#campo3_rutas').val()).val(rend_esperado);
    mapearControlesRecorrido();    
    calcularGlnsPorRuta();

}


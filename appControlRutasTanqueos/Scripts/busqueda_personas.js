﻿
var table_per = "tinytable_per", popup_per = "xf2";

$(document).ready(function () {

    agregarEventosTabla_per();

});

//-------------------Uso de JSON para conectarse a la BD por JavaScript------------
/*
var persona_bus = function (id_persona, nombres_apellidos) {
    this.id_persona = id_persona;
    this.nombres_apellidos = nombres_apellidos;
}*/

function getFormJsonPer(i) {
    var obj = new persona(i, $('#campo_filtrar_per').val());
    return obj;
}


function obtenerDatos_per(i, event) {
    //
    var obj = getFormJsonPer(i);//$(this).serializeObject(); //
    var nuevaFila = "";
    
    if ($('#campo_filtrar_per').val().trim() != "") {
        
        if (event.which != 13 && event.which != 38 && event.which != 40) {
            quitarTodaFilas_per();
           // alert("Hola " + obj.nombres_apellidos);
            $.ajax({ 
                url: '/Ruta/ajax_listaPersonas/',
                type: 'POST',
                contentType: 'application/json;charset=utf-8',
                data: JSON.stringify(obj),
                success: function (lista) {

                    $.each(lista, function (key, p) {

                        var cantfilas = $('#' + table_per + ' >tbody >tr').length;
                        nuevaFila = "<tr>";
                        nuevaFila += "<td>" + p.id_persona + "</td>";
                        nuevaFila += "<td>" + p.nombres_apellidos + "</td>";
                        nuevaFila += "<td>" + p.tarifa_combustible + "</td>";                        
                        nuevaFila += "</tr>";
                        $("#" + table_per).append(nuevaFila);
                        //alert("Hola");

                    });

                    agregarEventosTabla_per();
                    $("#" + table_per + " tbody tr:first").find("td").addClass('hover');

                },
                error: function (e) {
                    alert('Error: ' + e+"error 2 :::::"+ e.error);
                }
            });

        }
    }
    else {
        $('#campo_filtrar_per').select();
        $('#campo_filtrar_per').focus();
    }

    if (event.which == 38 || event.which == 40) moverseFilasTabla_per(event);
    if (event.which == 13) mostrarDatoSeleccionado_per();
    return false; //Esto anula el evento Submit()

}


function moverseFilasTabla_per(event) {

    var sw = 0;
    $('#' + table_per + ' tbody tr').each(function (key) {

        //alert($(this).find('td').eq(0).hasClass("hover"));
        if ($(this).find('td').eq(0).hasClass("hover")) {
            controlaSeleccion_per(key, event.which);
            sw = 1;
            return false;
        }

    });
    if (sw == 0) $("#" + table_per + " tbody tr:first").find("td").addClass('hover');
    return true;

}

function mostrarDatoSeleccionado_per() {

    $('#' + table_per + ' tbody tr').each(function (key) {

        //apopup_perlert("Hola");
        if ($(this).find('td').eq(0).hasClass("hover")) {
            mostrarDatos_per($(this).find("td").eq(0).html(), $(this).find("td").eq(1).html(), $(this).find("td").eq(2).html());
            ocultaPopup(popup_per);
            //xFenster.hideAll();
            //xFenster.instances[popup_per].hide();
            //desactivarSombraPantalla();
            quitarTodaFilas_per();
            return false;
        }

    });
    return true;

}

function ocultaPopup(popup) {
    //alert("Hola");
    xFenster.instances[popup].hide();
    desactivarSombraPantalla();
}

function quitarTodaFilas_per() {

    //alert("Holaaaa");
    var trs = $("#" + table_per + " tbody tr").length;
    for (var i = 0; i < trs; i++) {
        $("#" + table_per + " tbody tr").remove();
    }
}

function agregarEventosTabla_per() {

    $('#' + table_per + ' tbody tr').hover(

      function () {
          $('#' + table_per).find('tbody tr td').removeClass('hover');//Deseleccionamos todo
          $(this).find('td').addClass('hover');
      },
      function () {
          $(this).find('td').removeClass('hover');

      }

  );

    $('#' + table_per + ' tbody tr').click(function () {

        $('#' + table_per).find('tbody tr td').removeClass('hover');//Deseleccionamos todo
        $(this).find('td').addClass('hover');
        mostrarDatos_per($(this).find("td").eq(0).html(), $(this).find("td").eq(1).html(), $(this).find("td").eq(2).html());
        ocultaPopup(popup_per);
        quitarTodaFilas_per();

    });

}

function controlaSeleccion_per(pos, event) {
    
    $('#' + table_per + ' tbody tr').each(function (key) {

        //alert(key+""+event);
        if (key == pos - 1 && event == 38)//Tecla Hacia arriba
        {
            $('#' + table_per).find('tbody tr td').removeClass('hover');//Deseleccionamos todo
            $(this).find('td').addClass('hover');//Seleccionamos el anterior
            $("#campo_filtrar_per").val($(this).find("td").eq(1).html());
            $("#campo_filtrar_per").select();
            return false; 
        }
        else {
            if (key == pos + 1 && event == 40)//Tecla Hacia abajo
            {
                $('#' + table_per).find('tbody tr td').removeClass('hover');//Deseleccionamos todo
                $(this).find('td').addClass('hover');//Seleccionamos el siguiente
                $("#campo_filtrar_per").val($(this).find("td").eq(1).html());
                $("#campo_filtrar_per").select();
                return false;
            }

        }

    });

}

function mostrarDatos_per(id_persona, nombre, tarifa_combustible) {

    //alert($('#'+ $('#campo2_percepto').val()).val());
    //alert(id_persona+ "-"+ nombre)
    $('#' + $('#campo0_personas').val()).val(id_persona);
    $('#' + $('#campo1_personas').val()).val(nombre);
    $('#' + $('#campo2_personas').val()).val(tarifa_combustible);
    $('#' + $('#campo1_personas').val()).focus();
    $('#' + $('#campo1_personas').val()).select();
    //mapearControlesRecorrido();

}


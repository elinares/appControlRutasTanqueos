﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class distrito
    {
        public int id_distrito { get; set; }
        public string nombre { get; set; }

        public distrito()
        {
            this.id_distrito = 0;
        }

    }

    public class distritoViewModel
    {
        public int id_distrito { get; set; }
        public string nombre { get; set; }

        public distritoViewModel()
        {
            this.id_distrito = 0;
        }

    }

}
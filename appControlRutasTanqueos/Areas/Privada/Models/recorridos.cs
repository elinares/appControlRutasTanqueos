﻿using appControlRutasTanqueos.Servicios.Logica.Dtos;
using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
     public class recorrido
    {
        public int id_recorrido { get; set; }        
        public DateTime fecha { get; set; }
        public String fecha_cadena { get; set; }
        public unidad_transporte unidad_transporte { get; set; }
        public programacion programacion { get; set; }
        public persona conductor { get; set; }
        public persona cliente { get; set; }
        public mercaderia mercaderia { get; set; }
        public tanqueo tanqueo { get; set; }
        public ruta ruta { get; set; } // Define la ruta de origen y destino
        public interfaz interfaz { get; set; }

        public List<recorrido> lstR { get; set; }
        public List<recorridoDetalle> lstRD { get; set; }        
        public List<tanqueo> lstTan { get; set; }

        public recorrido() {

            this.id_recorrido = 0;
            this.interfaz = new interfaz();
            this.unidad_transporte = new unidad_transporte();
            this.programacion = new programacion();
            this.conductor = new persona();
            this.cliente = new persona();
            this.mercaderia = new mercaderia();
            this.ruta = new ruta();
            this.tanqueo = new tanqueo();
            this.interfaz = new interfaz();
            this.lstR = new List<recorrido>();
            this.lstRD = new List<recorridoDetalle>();

        }

    }

    public class recorridoViewModel
    {
        [Display(Name = "Codigo ", Prompt = "id_recorrido")]
        public int id_recorrido { get; set; }        
        
        [Display(Name = "Fecha ", Prompt = "fecha")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Digite fecha.")]
        [Date]
        public DateTime fecha { get; set; }
        public String fecha_cadena { get; set; }
        public unidad_transporte unidad_transporte { get; set; }
        public programacion programacion { get; set; }
        [Display(Name = "Conductor", Prompt = "conductor")]
        public persona conductor { get; set; }
        [Display(Name = "Cliente", Prompt = "cliente")]
        public persona cliente { get; set; }
        [Display(Name = "Mercaderia", Prompt = "mercaderia")]
        public mercaderia mercaderia { get; set; }
        [Display(Name = "Ruta", Prompt = "ruta")]
        public rutaViewModel ruta { get; set; } // Define la ruta de origen y destino
        public tanqueo tanqueo { get; set; }
        public interfazViewModel interfaz { get; set; }
        public List<recorrido> lstR { get; set; }
        public List<recorridoDetalle> lstRD { get; set; }
        public List<tanqueo> lstTan { get; set; }

        public recorridoViewModel()
        {

            this.id_recorrido = 0;
            this.fecha = DateTime.Now;
            this.interfaz = new interfazViewModel();
            this.unidad_transporte = new unidad_transporte();
            this.programacion = new programacion();
            this.conductor = new persona();
            this.cliente = new persona();
            this.mercaderia = new mercaderia();
            this.tanqueo = new tanqueo();
            this.ruta = new rutaViewModel();
            this.lstR = new List<recorrido>();
            this.lstRD = new List<recorridoDetalle>();
           

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class unidad_medida
    {
        public int id_unidad_medida { get; set; }
        public string nombre { get; set; }
        [Display(Name = "UM. MERCAD.", Prompt = "abreviatura")]
        public string abreviatura { get; set; }

        public unidad_medida() {

            this.id_unidad_medida = 0;

        }
    }
}
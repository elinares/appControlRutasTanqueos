﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class clase_documento
    {
        public int id_clase_documento { get; set; }
        public string nombre { get; set; }

        public clase_documento() {

            this.id_clase_documento = 0;
        }
    }
}
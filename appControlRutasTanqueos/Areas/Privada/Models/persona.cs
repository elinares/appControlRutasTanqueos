﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class persona
    {
        public int id_persona { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string nombres_apellidos { get; set; }
        public Decimal tarifa_combustible { get; set; }
        


        public interfaz interfaz { get; set; }
        public List<persona> lstPer { get; set; }

        public persona()
        {
            this.id_persona = 0;
            this.nombres_apellidos = "";
            this.nombres = "";
            this.interfaz = new interfaz();

        }
        

    }
}
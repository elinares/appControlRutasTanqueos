﻿using appControlRutasTanqueos.Servicios.Logica.Dtos;
using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class rendimiento
    {
        public int id_rendimiento { get; set; }
        public ruta ruta { get; set; }
        public decimal peso_minimo { get; set; }
        public decimal peso_maximo { get; set; }
        public decimal rend { get; set; } //Rendimiento
        public interfaz interfaz { get; set; }
        public List<rendimiento> lstRen { get; set; }
        
        public rendimiento()
        {
            this.id_rendimiento = 0;
            this.ruta = new ruta();
            this.peso_minimo = 0;
            this.peso_maximo = 0;
            this.rend = 0;
            this.interfaz = new interfaz();
        }

    }

    public class rendimientoViewModel
    {
        [Display(Name = "Código", Prompt = "id_rendimiento")]
        public int id_rendimiento { get; set; }
        public rutaViewModel ruta { get; set; }
        [Display(Name = "Peso Mín.", Prompt = "peso_minimo")]
        [Numeric]
        public decimal peso_minimo { get; set; }
        [Display(Name = "Peso Máx.", Prompt = "peso_maximo")]
        [Numeric]
        public decimal peso_maximo { get; set; }
        [Display(Name = "Rend.", Prompt = "rend")]
        [Numeric]
        public decimal rend { get; set; } //Rendimiento
        public interfazViewModel interfaz { get; set; }

        public List<rendimiento> lstRen { get; set; }

        public rendimientoViewModel()
        {
            this.id_rendimiento = 0;
            this.ruta = new rutaViewModel();
            this.peso_minimo = 0;
            this.peso_maximo = 0;
            this.rend = 0;
            this.interfaz = new interfazViewModel();
        }

    }
}
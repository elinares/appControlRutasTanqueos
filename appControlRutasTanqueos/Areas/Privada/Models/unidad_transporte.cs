﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class unidad_transporte
    {
        public int id_unidad_transporte { get; set; }
        [Display(Name = "Unidad", Prompt = "placa")]
        public string placa { get; set; }
        public string modelo { get; set; }
        [Display(Name = "Conductor", Prompt = "conductor")]
        public persona conductor { get; set; }
        public List<unidad_transporte> lstUT { get; set; }

        public unidad_transporte() {

            this.id_unidad_transporte = 0;
            this.conductor = new persona();
            this.lstUT = new List<unidad_transporte>();

        }
    }
}
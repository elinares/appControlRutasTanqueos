﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class programacion
    {
        [Display(Name = "Prog.", Prompt = "id_programacion")]
        public int id_programacion { get; set; }
        public usuario usuario { get; set; }
        public DateTime fecha { get; set; }
        
        public programacion() {
            this.id_programacion = 0;
            this.usuario = new usuario();
        }
    }
}
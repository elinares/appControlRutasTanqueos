﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAnnotationsExtensions;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class interfaz
    {
        public int id { get; set; }
        public String tipo_operacion { get; set; }
        public String titulo { get; set; }
        public String title { get; set; }
        public String subtitulo { get; set; }
        public String mensaje { get; set; }
        public int contador { get; set; }
        public usuario usuario { get; set; }
        public DateTime fecha_registro { get; set; }
        public Boolean estado { get; set; }        
        public DateTime fecha_inicial { get; set; }        
        public DateTime fecha_final { get; set; }

        public interfaz()
        {

            this.usuario = new usuario();
            this.fecha_final = DateTime.Now;
            this.fecha_inicial = DateTime.Now;
            this.fecha_registro = DateTime.Now;
            this.contador = 0;
            this.tipo_operacion = "1";
            this.estado = true;
            this.titulo = "Control de Rutas y Tanqueos";
            this.title = "CONTROL DE RUTAS Y TANQUEOS - TVN S.A.C.";
        }


    }

    public class interfazViewModel
    {
        public int id { get; set; }
        public String tipo_operacion { get;set;}
        public String titulo {get;set;}
        public String title { get; set; }
        public String subtitulo { get; set; }
        public String mensaje { get; set; }
        public int contador { get;set;}
        public usuario usuario { get; set; }
        [Date]
        public DateTime fecha_registro { get; set; }
        public Boolean estado { get; set; }

        [Display(Name = "Fecha inicial", Prompt = "fecha_inicial")]
        [Required(ErrorMessage = "Digite fecha inicial.")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",ApplyFormatInEditMode=true)]
        [Date]
        public DateTime fecha_inicial { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha final", Prompt = "fecha_final")]
        [Required(ErrorMessage = "Digite fecha final.")]
        [Date]        
        public DateTime fecha_final { get; set; }

        public interfazViewModel() {

            this.usuario = new usuario();
            this.fecha_final = DateTime.Now;
            this.fecha_inicial = DateTime.Now;
            this.fecha_registro = DateTime.Now;
            this.contador = 0;
            this.tipo_operacion = "1";
            this.estado = true;
            this.titulo = "Control de Rutas y Tanqueos";
            this.title = "CONTROL DE RUTAS Y TANQUEOS - TVN S.A.C.";
        }


    }
}
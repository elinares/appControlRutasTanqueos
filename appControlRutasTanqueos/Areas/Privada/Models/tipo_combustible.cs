﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class tipo_combustible
    {
        public int id_tipo_combustible { get; set; }
        [Display(Name = "Tipo Combus.", Prompt = "nombre")]    
        public string nombre { get; set; }

        public tipo_combustible() {

            this.id_tipo_combustible = 0;
            this.nombre = "PETR";

        }

    }
}
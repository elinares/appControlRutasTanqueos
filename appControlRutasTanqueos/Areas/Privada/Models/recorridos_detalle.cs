﻿using appControlRutasTanqueos.Servicios.Logica.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class recorridoDetalle
    {
        public int id_recorrido_detalle { get; set; }
        public recorrido recorrido { get; set; }
        public ruta trayectoria { get; set; } // Define los recorridos dentro de la ruta
        public List<String> lstAct { get; set; }//Lista de actividades: Descarga mercaderia origen, Descarga mercaderia en destino, Tanqueo en origen, Tanqueo en destino, etc                
        public tanqueo tanqueo { get; set; }


        /*----Nuevo campos------*/
        public List<tanqueo> lstTan { get; set; }
        public Decimal km_recor_esp { get; set; }
        public Decimal km_recor_real { get; set; }
        public Decimal nro_galones_abast { get; set; }//Almacena el total de galones abastecidos en un recorrido con uno o mas tanqueos
        public Decimal nro_galones_asig { get; set; }//Almacena el total de galones asignados en un recorrido con uno o mas tanqueos
        public Decimal nro_galones_consum { get; set; }

        public Decimal dif_km { get; set; }
        public Decimal dif_glns { get; set; }
        public Decimal dif_rend { get; set; }

        public interfaz interfaz { get; set; }
        public List<recorridoDetalle> lstRD { get; set; }


        public recorridoDetalle()
        {

            this.id_recorrido_detalle = 0;
            this.km_recor_esp = 0;
            this.km_recor_real = 0;
            this.nro_galones_abast = 0;
            this.nro_galones_asig = 0;
            this.dif_glns = 0;
            this.dif_km = 0;
            this.recorrido = new recorrido();
            this.trayectoria = new ruta();
            this.tanqueo = new tanqueo();
            this.interfaz = new interfaz();
            this.lstAct = new List<String>();
            
        }



    }
    public class recorridoDetalleViewModel
    {
        public int id_recorrido_detalle { get; set; }
        public recorridoViewModel recorrido { get; set; }
        public rutaViewModel trayectoria { get; set; } // Define los recorridos dentro de la ruta
        public List<String> lstAct { get; set; }//Lista de actividades: Descarga mercaderia origen, Descarga mercaderia en destino, Tanqueo en origen, Tanqueo en destino, etc                
        [Display(Name = "Tanqueo ", Prompt = "tanqueo")]
        public tanqueoViewModel tanqueo { get; set; }
        public interfaz interfaz { get; set; }
        [Display(Name = "Km. Esper.", Prompt = "km_recor_esp")]
        public Decimal km_recor_esp { get; set; }
        [Display(Name = "Km. Real.", Prompt = "km_recor_real")]        
        public Decimal km_recor_real { get; set; }
        [Display(Name = "Glns.Abast.", Prompt = "nro_galones_abast")]        
        public Decimal nro_galones_abast { get; set; }//Almacena el total de galones abastecidos en un recorrido con uno o mas tanqueos
        [Display(Name = "Glns.Asig.", Prompt = "nro_galones_asig")]          
        public Decimal nro_galones_asig { get; set; }//Almacena el total de galones asignados en un recorrido con uno o mas tanqueos
        [Display(Name = "Glns.Consum.", Prompt = "nro_galones_consum")]          
        public Decimal nro_galones_consum { get; set; }

        public Decimal dif_km { get; set; }
        public Decimal dif_glns { get; set; }
        public Decimal dif_rend { get; set; }

        public List<recorridoDetalle> lstRD { get; set; }



        public recorridoDetalleViewModel()
        {

            this.id_recorrido_detalle = 0;
            this.km_recor_esp = 0;
            this.km_recor_real = 0;
            this.nro_galones_abast = 0;
            this.nro_galones_asig = 0;
            this.dif_glns = 0;
            this.dif_km = 0;
            this.recorrido = new recorridoViewModel();
            this.trayectoria = new rutaViewModel();
            this.tanqueo = new tanqueoViewModel();
            this.interfaz = new interfaz();
            this.lstAct = new List<String>();


        }


    }
}
﻿using appControlRutasTanqueos.Servicios.Logica.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{

    public class ruta
    {
        public int id_ruta { get; set; }
        public String nombre { get; set; }
        public decimal tiempo_aprox { get; set; }
        public decimal km_aprox { get; set; }
        public decimal rend_esperado { get; set; }
        public decimal peso { get; set; }
        public distrito ciudad_origen { get; set; }
        public distrito ciudad_destino { get; set; }

        public List<ruta> lstRut { get; set; }
        public interfaz interfaz { get; set; }

        public ruta()
        {
            this.id_ruta = 0;
            this.interfaz = new interfaz();
            this.ciudad_destino = new distrito();
            this.ciudad_origen = new distrito();
            this.tiempo_aprox = 0;
            this.km_aprox = 0;
            this.peso = 0;
            this.lstRut = new List<ruta>();
        }


    }

    public class rutaViewModel
    {
        public int id_ruta { get; set; }

        [Display(Name = "Ruta", Prompt = "nombre")]
        public String nombre { get; set; }
        [Display(Name = "Origen", Prompt = "ciudad_origen")]
        public distritoViewModel ciudad_origen { get; set; }
        [Display(Name = "Destino", Prompt = "ciudad_destino")]
        public distritoViewModel ciudad_destino { get; set; }
        public decimal tiempo_aprox { get; set; }
        public decimal km_aprox { get; set; }
        public decimal peso { get; set; }
        public decimal rend_esperado { get; set; }

        public List<ruta> lstRut { get; set; }
        public interfaz interfaz { get; set; }


        public rutaViewModel()
        {
            this.id_ruta = 0;
            this.interfaz = new interfaz();
            this.ciudad_destino = new distritoViewModel();
            this.ciudad_origen = new distritoViewModel();
            this.tiempo_aprox = 0;
            this.km_aprox = 0;
            this.peso = 0;
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Areas.Privada.Models
{
    public class mercaderia
    {
        public int id_mercaderia { get; set; }
        [Display(Name = "Mercaderia", Prompt = "nombre")]
        public string nombre { get; set; }
        [Display(Name = "Peso", Prompt = "peso")]
        public decimal peso { get; set; }
        public unidad_medida unidad_medida { get; set; }
        public persona cliente { set; get; } //Propietario de la mercaderia

        public mercaderia() {

            this.unidad_medida = new unidad_medida();
            this.cliente = new persona();
            this.peso = 0;
        }
    }
}
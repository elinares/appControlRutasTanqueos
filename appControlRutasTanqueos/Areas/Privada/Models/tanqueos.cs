﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace appControlRutasTanqueos.Areas.Privada.Models
{

    public class tanqueo
    {
       
        public int id_tanqueo { get; set; }
        public clase_documento clase_documento {get;set; }
        public int id_recorrido_detalle { get; set; }      

        public string serie { get; set; }
        public string numero { get; set; } 
        public string serie_numero { get; set; }
        public decimal nro_galones_anterior { get; set; }
        public decimal nro_galones_abast {get;set; }
        public decimal nro_galones_asig { get; set; }
        public decimal kilometraje_anterior { get; set; }
        public decimal kilometraje {get;set; } 
        public decimal rend_real { get; set; }// Rendimiento = Km. recorridos / nro_galones tanqueados
        public decimal rend_esperado {get;set;}// Rendimiento = Km. recorridos / nro_galones tanqueados        

        [Display(Name = "Fecha Tanq.", Prompt = "fecha")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Digite fecha.")]
        [Date]
        public DateTime fecha { get; set; }
        public String hora { get; set; }
        public String minuto { get; set; }

        public decimal precio { get; set; }
        public decimal total { get; set; }
        public persona proveedor { get; set; }
        public distrito distrito { get; set; }//Ludar donde se tanqueo.
        public tipo_combustible tipo_combustible { get; set; } //GASOLINA O PETROLEO
        public string tipo_tanqueo { get; set; } //FULL o PARCIAL


        public int id_programacion { get; set; }
        public unidad_transporte unidad_transporte { get; set; }
        public persona empresa { get; set; }
        public String glosa { get; set; }
        public interfaz interfaz { get; set; }
        public Boolean ind_nodesemb { get; set; }
        public String detalle_ruta { get; set; }
        public ruta ruta { get; set; }
        public ruta ruta_retorno { get; set; }
        public Boolean ind_ficticio { get; set; }

        public List<tanqueo> lstTan { get; set; }

        public tanqueo() {

            this.id_tanqueo = 0;
            this.nro_galones_anterior = 0;
            this.kilometraje = 0;
            this.kilometraje_anterior = 0;
            this.nro_galones_abast = 0;
            this.nro_galones_asig = 0;
            this.precio = 0;
            this.rend_esperado = 0;
            this.rend_real = 0;
            this.fecha = DateTime.Now;
            this.hora = "00";            
            this.minuto = "00";
            this.serie = "";
            this.numero = "";
            this.serie_numero = "";
            this.detalle_ruta = "";
            this.glosa = "";
            this.tipo_tanqueo = "FUL";
            this.ind_ficticio = false;
            this.ind_nodesemb = false;
            this.clase_documento = new clase_documento();
            this.proveedor = new persona();
            this.distrito = new distrito();
            this.tipo_combustible = new tipo_combustible();
            this.interfaz = new interfaz();

            this.unidad_transporte = new unidad_transporte();
            this.empresa = new persona();
            this.ruta = new ruta();
            this.ruta_retorno = new ruta();

            
        }

    }

    public class tanqueoViewModel
    {
       
        public int id_tanqueo { get; set; }
        /*--Ticket que genera el grifo al momento de provisonal de combustible la unidad--*/
        public clase_documento clase_documento {get;set; }
        public int id_recorrido_detalle { get; set; }      
        public string serie { get; set; }
        public string numero { get; set; }
        [Display(Name = "N° Vale", Prompt = "serie_numero")]    
        public string serie_numero { get; set; }
        //------------------------------------------------
        [Display(Name = "Glns.Ant.Tanq.", Prompt = "nro_galones_anterior")]    
        public decimal nro_galones_anterior { get; set; }
        [Display(Name = "Glns. Abast.", Prompt = "nro_galones_abast")]    
        public decimal nro_galones_abast {get;set; }
        [Display(Name = "Glns. Asig.", Prompt = "nro_galones_asig")]
        public decimal nro_galones_asig { get; set; }
        [Display(Name = "Km.Ant.", Prompt = "kilometraje_anterior")]   
        public decimal kilometraje_anterior { get; set; }
        [Display(Name = "Km.", Prompt = "kilometraje")]   
        public decimal kilometraje {get;set; }
        [Display(Name = "Rend. Real", Prompt = "rend_real")]   
        public decimal rend_real { get; set; }// Rendimiento = Km. recorridos / nro_galones tanqueados
        [Display(Name = "Rend. Esper.", Prompt = "rend_esperado")]
        public decimal rend_esperado {get;set;}// Rendimiento = Km. recorridos / nro_galones tanqueados        

        [Display(Name = "Fecha Tanq.", Prompt = "fecha")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Digite fecha.")]
        [Date]
        public DateTime fecha { get; set; }
        public String hora { get; set; }
        public String minuto { get; set; }
        [Display(Name = "Precio", Prompt = "precio")]
        public decimal precio { get; set; }
        [Display(Name = "Total", Prompt = "total")]
        public decimal total { get; set; }
        public persona proveedor { get; set; }
        public distritoViewModel distrito { get; set; }//Ludar donde se tanqueo.
        public tipo_combustible tipo_combustible { get; set; } //GASOLINA O PETROLEO
        [Display(Name = "Tipo Tanq.", Prompt = "tipo_tanqueo")]    
        public string tipo_tanqueo { get; set; } //FULL o PARCIAL


        public int id_programacion { get; set; }
        public unidad_transporte unidad_transporte { get; set; }
        public persona empresa { get; set; }
        public String glosa { get; set; }
        public interfazViewModel interfaz { get; set; }
        public Boolean ind_nodesemb { get; set; }
        public String detalle_ruta { get; set; }
        public ruta ruta { get; set; }
        public ruta ruta_retorno { get; set; }
        public Boolean ind_ficticio { get; set; }

        public List<tanqueo> lstTan { get; set; }

        public tanqueoViewModel()
        {
            this.id_tanqueo = 0;
            this.kilometraje = 0;
            this.kilometraje_anterior = 0;
            this.nro_galones_abast = 0;
            this.nro_galones_asig = 0;
            this.nro_galones_anterior = 0;
            this.precio = 0;
            this.rend_esperado = 0;
            this.rend_real = 0;
            this.hora = "00";
            this.minuto = "00";
            this.serie = "";
            this.numero = "";
            this.serie_numero = "";
            this.detalle_ruta = "";
            this.glosa = "";
            this.ind_ficticio = false;
            this.ind_nodesemb = false;
            this.tipo_tanqueo = "FUL";
            this.clase_documento = new clase_documento();
            this.proveedor = new persona();
            this.distrito = new distritoViewModel();
            this.tipo_combustible = new tipo_combustible();
            this.interfaz = new interfazViewModel();

            this.unidad_transporte = new unidad_transporte();
            this.empresa = new persona();
            this.ruta = new ruta();
            this.ruta_retorno = new ruta();
        }

    }
}
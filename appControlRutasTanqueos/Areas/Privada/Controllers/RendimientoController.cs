﻿using appControlRutasTanqueos.Areas.Privada.Models;
using appControlRutasTanqueos.Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using appControlRutasTanqueos.Servicios.Logica;
using AutoMapper;
using appControlRutasTanqueos.Servicios.Logica.Dtos;
using appControlRutasTanqueos.Models;

namespace appControlRutasTanqueos.Areas.Privada.Controllers
{
    public class RendimientoController : Controller
    {
        private readonly ServicioRendimiento servicioRendimiento;

        private interfaz objInt;        
        private ViewModelBuilder builder;
        private rendimiento obj;
        private rendimientoDto objDto;

        public RendimientoController() {

            servicioRendimiento = new ServicioRendimiento();
            builder = new ViewModelBuilder();

            objInt = new interfaz();
            obj = new rendimiento();
            objDto = new rendimientoDto();

            ViewBag.Titulo = objInt.titulo;
            ViewBag.Title = objInt.title;
            ViewBag.Subtitulo = "Plantilla de rendimiento por ruta y peso transportado";

        }

        [HttpGet]
        public ActionResult RendimientosListar()
        {

            try
            {
                
                objDto = Mapper.Map<rendimiento, rendimientoDto>(obj);
                objDto = servicioRendimiento.listar(objDto);
                obj = Mapper.Map<rendimientoDto,rendimiento>(objDto);
                return View("RendimientosListar", builder.RendimientoViewModel(obj)); //Otra forma

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }
            
        }


        [HttpPost]
        public ActionResult RendimientosListar(rendimiento  p_obj)
        {            

            try
            {
                objDto = Mapper.Map<rendimiento, rendimientoDto>(p_obj);
                objDto = servicioRendimiento.listar(objDto);
                p_obj = Mapper.Map<rendimientoDto, rendimiento>(objDto);
                return View("RendimientosListar", builder.RendimientoViewModel(p_obj)); //Otra forma


            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }

        [HttpGet]
        public ActionResult RendimientoCRUD(String tipo_operacion, int id, int id_ruta)
        {

            try
            {

                objDto.id_rendimiento = id;
                objDto.ruta.id_ruta = id_ruta;               
                objDto = servicioRendimiento.obtener(objDto);
                obj = Mapper.Map<rendimientoDto, rendimiento>(objDto);
                obj.interfaz.tipo_operacion = tipo_operacion;
                if (obj.lstRen.Count==0) {
                    obj.lstRen = new List<rendimiento>();
                    obj.lstRen.Add(new rendimiento());
                }
                return View("RendimientosCRUD", builder.RendimientoViewModel(obj)); //Otra forma                

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }

        [HttpPost]
        public ActionResult RendimientoCRUD(rendimiento p_obj)
        {
            try
            {
                
                // Model Valider, Para validar los objetos1
                if (!ModelState.IsValid) throw new Exception();                
                objDto = Mapper.Map<rendimiento, rendimientoDto>(p_obj);
                objDto = servicioRendimiento.generar_iae(objDto);
                p_obj = Mapper.Map<rendimientoDto, rendimiento>(objDto);  
                return RedirectToAction("RendimientosListar");

                /*OTRA FORMA :METODO TRADICIONAL UTILIZANDO WEB FROM */
                /*String[] id_rendimiento = Request.Params.Get("item.id_rendimiento").Split(',');
                String[] id_ruta = Request.Params.Get("ruta.id_ruta").Split(',');
                String[] peso_minimo = Request.Params.Get("item.peso_minimo").Split(',');
                String[] peso_maximo = Request.Params.Get("item.peso_maximo").Split(',');                
                String[] rend = Request.Params.Get("item.rend").Split(',');  
                 for (int i = 0; i < peso_minimo.Length; i++)
                 {
                     p_obj.id_rendimiento = Convert.ToInt32(id_rendimiento[i]);
                     p_obj.ruta.id_ruta = Convert.ToInt32(id_ruta[i]);
                     p_obj.peso_minimo = Convert.ToDecimal(peso_minimo[i]);
                     p_obj.peso_maximo = Convert.ToDecimal(peso_maximo[i]);
                     p_obj.rend = Convert.ToDecimal(rend[i]);
                     //obj = builder.Rendimiento(p_obj);
                     objDto = Mapper.Map<rendimiento, rendimientoDto>(p_obj);
                     objDto = servicioRendimiento.generar_iae(objDto);
                     p_obj = Mapper.Map<rendimientoDto, rendimiento>(objDto);  
                
                 }*/


            }
            catch (Exception e)
            {
                p_obj.id_rendimiento = 0;
                p_obj.interfaz.tipo_operacion = "1";
                objDto = Mapper.Map<rendimiento, rendimientoDto>(p_obj);
                objDto = servicioRendimiento.obtener(objDto);
                p_obj = Mapper.Map<rendimientoDto, rendimiento>(objDto);
                p_obj.interfaz.mensaje = e.Message;
                return PartialView("RendimientosCRUD", builder.RendimientoViewModel(p_obj)); //Otra forma              
            }
            finally
            {

            }


        }


    }
}

﻿using appControlRutasTanqueos.Areas.Privada.Models;
using appControlRutasTanqueos.Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using appControlRutasTanqueos.Servicios.Logica;
using AutoMapper;
using appControlRutasTanqueos.Servicios.Logica.Dtos;
using appControlRutasTanqueos.Models;



namespace appControlRutasTanqueos.Areas.Privada.Controllers
{
    public class RutaController : Controller
    {
        private readonly ServicioRuta servicioRuta;

        private interfaz objInt;        
        private ViewModelBuilder builder;
        private ruta obj;
        private rutaDto objDto;
        private persona objPer;
        private personaDto objPerDto;

        public RutaController()
        {

            servicioRuta = new ServicioRuta();
            builder = new ViewModelBuilder();

            objInt = new interfaz();
            obj = new ruta();
            objDto = new rutaDto();
            objPer=new persona();
            objPerDto=new personaDto();

            ViewBag.Titulo = objInt.titulo;
            ViewBag.Title = objInt.title;
            ViewBag.Subtitulo = "Rutas";

        }

        [HttpPost]
        public JsonResult ajax_listaRutas( ruta p_obj)
        {

            try
            {

                objDto = Mapper.Map<ruta, rutaDto>(p_obj);
                objDto = servicioRuta.listar(objDto);
                obj = Mapper.Map<rutaDto, ruta>(objDto);
                //View("RpteTrazaAbastCombust", builder.RutaViewModel(obj)); //Otra forma
               

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

            return Json(builder.RutaViewModel(obj).lstRut, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        public JsonResult ajax_listaPersonas(persona p_obj)
        {

            try
            {

                objPerDto = Mapper.Map<persona, personaDto>(p_obj);
                objPerDto = servicioRuta.listar_persona(objPerDto);
                objPer = Mapper.Map<personaDto, persona>(objPerDto);
                //View("RpteTrazaAbastCombust", builder.RutaViewModel(obj)); //Otra forma

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

            return Json(objPer.lstPer, JsonRequestBehavior.AllowGet);


        }



    }
}

﻿
using appControlRutasTanqueos.Areas.Privada.Models;
using appControlRutasTanqueos.Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using appControlRutasTanqueos.Servicios.Logica;
using AutoMapper;
using appControlRutasTanqueos.Servicios.Logica.Dtos;
using appControlRutasTanqueos.Models;

/*--Referencias para la exportacion a excel--*/
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Reflection;
/*------------------------------------------*/

namespace appControlRutasTanqueos.Areas.Privada.Controllers
{
    public class RecorridoController : Controller
    {
        private readonly ServicioRecorrido servicioRecorrido;
        private  ServicioRecorridoDetalle servicioRecorridoDetalle;
        private  ServicioTanqueo servicioTanqueo;

        private interfaz objInt;        
        private ViewModelBuilder builder;
        private recorridoDetalle obj;
        private recorridoDetalleDto objDto;
        private recorridoDto objRDto;
        private recorrido objR;
        private tanqueoDto objTanDto;
        private tanqueo objTan;
        

        public RecorridoController()
        {

            servicioRecorrido = new ServicioRecorrido();            
            builder = new ViewModelBuilder();

            objInt = new interfaz();
            obj = new recorridoDetalle();
            objDto = new recorridoDetalleDto();
            objRDto = new recorridoDto();
            objR = new recorrido();
            objTanDto = new tanqueoDto();
            objTan = new tanqueo();
            
            ViewBag.Titulo = objInt.titulo;
            ViewBag.Title = objInt.title;
            ViewBag.Subtitulo = "Control de kilometraje y consumo por ruta recorrida";

        }

        [HttpGet]
        public ActionResult RecorridosListar()
        {

            try
            {

                objRDto = Mapper.Map<recorrido, recorridoDto>(objR);
                objRDto = servicioRecorrido.obtener(objRDto);
                objR = Mapper.Map<recorridoDto, recorrido>(objRDto);
                return View("RecorridosListar", builder.RecorridoViewModel(objR)); //Otra forma

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }

        [HttpPost]
        public ActionResult RecorridosListar(recorrido p_obj)
        {

            try
            {

                objRDto = Mapper.Map<recorrido, recorridoDto>(p_obj);
                objRDto = servicioRecorrido.obtener(objRDto);
                p_obj = Mapper.Map<recorridoDto, recorrido>(objRDto);
                return View("RecorridosListar", builder.RecorridoViewModel(p_obj)); //Otra forma

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }




        [HttpGet]
        public ActionResult RecorridosCRUD(String tipo_operacion, int id)
        {

            try
            {

                objRDto.id_recorrido = id;
                objRDto = servicioRecorrido.obtenerConDetalle(objRDto);
                objR = Mapper.Map<recorridoDto, recorrido>(objRDto);
                objR.interfaz.tipo_operacion = tipo_operacion;                
                return View("RecorridosCRUD", builder.RecorridoViewModel(objR)); //Otra forma                

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }

        [HttpPost]
        public ActionResult RecorridosCRUD(recorrido p_obj)
        {
            try
            {

                // Model Valider, Para validar los objetos1
                if (!ModelState.IsValid) throw new Exception("");
                objRDto = Mapper.Map<recorrido, recorridoDto>(p_obj);
                objRDto = servicioRecorrido.generar_iae(objRDto);
                p_obj = Mapper.Map<recorridoDto, recorrido>(objRDto);
                return RedirectToAction("RecorridosListar");


            }
            catch (Exception e)
            {
                p_obj.id_recorrido = 0;
                p_obj.interfaz.tipo_operacion = "1";
                objRDto = Mapper.Map<recorrido, recorridoDto>(p_obj);
                objRDto = servicioRecorrido.obtener(objRDto);
                p_obj = Mapper.Map<recorridoDto, recorrido>(objRDto);
                p_obj.interfaz.mensaje = e.Message;
                return PartialView("RecorridosCRUD", builder.RecorridoViewModel(p_obj)); //Otra forma  
            
            }
            finally
            {

            }


        }



        [HttpGet]
        public ActionResult RpteTrazaAbastCombust()
        {

            try
            {
                servicioRecorridoDetalle = new ServicioRecorridoDetalle();
                objDto = Mapper.Map<recorridoDetalle, recorridoDetalleDto>(obj);
                objDto = servicioRecorridoDetalle.listar_traza_recorrido_detalle(objDto);
                obj = Mapper.Map<recorridoDetalleDto, recorridoDetalle>(objDto);
                return View("RpteTrazaAbastCombust", builder.RecorridoDetalleViewModel(obj)); //Otra forma

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }

        [HttpPost]
        public ActionResult RpteTrazaAbastCombust(recorridoDetalleViewModel p_obj)
        {            

            try
            {
                ServicioRecorridoDetalle servicioRecorridoDetalle = new ServicioRecorridoDetalle();
                obj = builder.RecorridoDetalle(p_obj);
                objDto = Mapper.Map<recorridoDetalle, recorridoDetalleDto>(obj);
                objDto = servicioRecorridoDetalle.listar_traza_recorrido_detalle(objDto);
                obj = Mapper.Map<recorridoDetalleDto, recorridoDetalle>(objDto);
                return View("RpteTrazaAbastCombust", builder.RecorridoDetalleViewModel(obj)); //Otra forma


            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }




        [HttpGet]
        public ActionResult RpteGlnsAsigVSGlnsAbast()
        {

            try
            {
                ViewBag.Subtitulo = "Galones asignados VS galones abastecidos";
                servicioRecorridoDetalle = new ServicioRecorridoDetalle();
                obj.interfaz.tipo_operacion = "2";
                objDto = Mapper.Map<recorridoDetalle, recorridoDetalleDto>(obj);
                objDto = servicioRecorridoDetalle.obtener(objDto);
                obj = Mapper.Map<recorridoDetalleDto, recorridoDetalle>(objDto);
                return View("RpteGlnsAsigVSGlnsAbast", builder.RecorridoDetalleViewModel(obj)); //Otra forma

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }

        [HttpPost]
        public ActionResult RpteGlnsAsigVSGlnsAbast(recorridoDetalle p_obj)
        {
            try
            {
                ViewBag.Subtitulo = "Galones asignados VS galones abastecidos";
                servicioRecorridoDetalle = new ServicioRecorridoDetalle();
                objDto = Mapper.Map<recorridoDetalle, recorridoDetalleDto>(p_obj);
                objDto = servicioRecorridoDetalle.obtener(objDto);
                obj = Mapper.Map<recorridoDetalleDto, recorridoDetalle>(objDto);
                return View("RpteGlnsAsigVSGlnsAbast", builder.RecorridoDetalleViewModel(obj)); //Otra forma
            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }



        [HttpGet]
        public ActionResult RpteDsctoConductores()
        {

            try
            {
                ViewBag.Subtitulo = "Descuento a conductores por consumo excesivo de combustible";
                servicioTanqueo = new ServicioTanqueo();
                objTan.interfaz.tipo_operacion = "2";
                objTanDto = Mapper.Map<tanqueo, tanqueoDto>(objTan);
                objTanDto = servicioTanqueo.listar(objTanDto);
                objTan = Mapper.Map<tanqueoDto, tanqueo>(objTanDto);
                return View("RpteDsctoConductores", builder.TanqueoViewModel(objTan)); //Otra forma

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }

        [HttpPost]
        public ActionResult RpteDsctoConductores(tanqueo p_obj)
        {

            try
            {
                ViewBag.Subtitulo = "Descuento a conductores por consumo excesivo de combustible";
                servicioTanqueo = new ServicioTanqueo();
                objTanDto = Mapper.Map<tanqueo, tanqueoDto>(p_obj);
                objTanDto = servicioTanqueo.listar(objTanDto);
                objTan = Mapper.Map<tanqueoDto, tanqueo>(objTanDto);
                return View("RpteDsctoConductores", builder.TanqueoViewModel(objTan)); //Otra forma

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

        }



        [HttpPost]
        public JsonResult ajax_listaDatosProgramacion(recorrido p_obj)
        {

            try
            {
                objRDto = Mapper.Map<recorrido, recorridoDto>(p_obj);
                objRDto.interfaz.tipo_operacion = "2";
                objRDto = servicioRecorrido.obtenerJson(objRDto);
                objR = Mapper.Map<recorridoDto, recorrido>(objRDto);
                //objR.lstR[0].lstR = new List<recorrido>();
            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {

            }

            return Json(builder.RecorridoViewModel(objR).lstR , JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult exportarRpteTrazaAbastCombust(recorridoDetalleViewModel p_obj)
        {
            MemoryStream ms = new MemoryStream();
            FileStream fs;
            HSSFWorkbook templateWorkbook;
            ISheet sheet;
            IRow dataRow;
            int colum = 1;
            int fila = 6;
            String ruta = "\\Reportes\\";
            String file = "RpteTrazaAbastCombust.xls";
            String hoja = "Hoja1";

            try
            {

                servicioRecorridoDetalle = new ServicioRecorridoDetalle();
                obj = builder.RecorridoDetalle(p_obj);
                objDto = Mapper.Map<recorridoDetalle, recorridoDetalleDto>(obj);
                objDto = servicioRecorridoDetalle.listar_traza_recorrido_detalle(objDto);
                obj = Mapper.Map<recorridoDetalleDto, recorridoDetalle>(objDto);

                /*---------------------------------------------EXPORTACION -------------------------------------*/
                fs = new FileStream(Server.MapPath("~") + ruta + file, FileMode.Open, FileAccess.Read);
                templateWorkbook = new HSSFWorkbook(fs, true);
                sheet = templateWorkbook.GetSheet(hoja);

                int index = 0;
                foreach (recorridoDetalle item in  obj.lstRD )
                {
                    colum = 1;
                    dataRow = sheet.GetRow(fila); if (dataRow.esNulo()) dataRow = sheet.CreateRow(fila); fila++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(++index); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.tanqueo.fecha); colum++;

                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.unidad_transporte.placa); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.unidad_transporte.conductor.nombres_apellidos); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.mercaderia.cliente.nombres_apellidos); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.recorrido.mercaderia.peso); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.ruta.ciudad_origen.nombre); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.ruta.ciudad_destino.nombre); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.trayectoria.nombre); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.tanqueo.proveedor.nombres_apellidos + "-" + item.tanqueo.distrito.nombre); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.tanqueo.kilometraje); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.tanqueo.kilometraje_anterior); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)(item.tanqueo.kilometraje - item.tanqueo.kilometraje_anterior)); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.tanqueo.rend_esperado); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.tanqueo.rend_real); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.tanqueo.nro_galones_asig); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.tanqueo.nro_galones_abast); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)(item.tanqueo.nro_galones_asig-item.tanqueo.nro_galones_abast)); colum++;

                }
               
                sheet.ForceFormulaRecalculation = true;
                templateWorkbook.Write(ms);
                TempData["Message"] = "Excel report created successfully!";
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {

            }
            return File(ms.ToArray(), "application/vnd.ms-excel", file);

        }





        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult exportarRpteGlnsAsigVSGlnsAbast(recorridoDetalle p_obj)
        {
            MemoryStream ms = new MemoryStream();
            FileStream fs;
            HSSFWorkbook templateWorkbook;
            ISheet sheet;
            IRow dataRow;
            int colum = 1;
            int fila = 6;
            String ruta = "\\Reportes\\";
            String file = "RpteGlnsAsigVSGlnsAbast.xls";
            String hoja = "Hoja1";

            try
            {

                servicioRecorridoDetalle = new ServicioRecorridoDetalle();
                //obj = builder.RecorridoDetalle(p_obj);
                objDto = Mapper.Map<recorridoDetalle, recorridoDetalleDto>(p_obj);
                objDto = servicioRecorridoDetalle.obtener(objDto);
                obj = Mapper.Map<recorridoDetalleDto, recorridoDetalle>(objDto);

                /*---------------------------------------------EXPORTACION -------------------------------------*/
                fs = new FileStream(Server.MapPath("~") + ruta + file, FileMode.Open, FileAccess.Read);
                templateWorkbook = new HSSFWorkbook(fs, true);
                sheet = templateWorkbook.GetSheet(hoja);

                int index = 0;
                foreach (recorridoDetalle item in obj.lstRD)
                {
                    colum = 1;
                    dataRow = sheet.GetRow(fila); if (dataRow.esNulo()) dataRow = sheet.CreateRow(fila); fila++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(++index); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.fecha); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.unidad_transporte.placa); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.programacion.id_programacion); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.unidad_transporte.conductor.nombres_apellidos); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.cliente.nombres_apellidos); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.recorrido.mercaderia.peso); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.ruta.ciudad_origen.nombre); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.recorrido.ruta.ciudad_destino.nombre); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.trayectoria.nombre); colum++;                    
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.km_recor_esp); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.km_recor_real); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.dif_km); colum++;

                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.nro_galones_asig); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.nro_galones_abast); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.dif_glns); colum++;

                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.tanqueo.rend_esperado); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.tanqueo.rend_real); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.dif_rend); colum++;
                    

                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.nro_galones_consum); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.tanqueo.precio); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.tanqueo.total); colum++;


                }

                sheet.ForceFormulaRecalculation = true;
                templateWorkbook.Write(ms);
                TempData["Message"] = "Excel report created successfully!";
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {

            }
            return File(ms.ToArray(), "application/vnd.ms-excel", file);

        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult exportarRpteDsctoConductores(tanqueo p_obj)
        {
            MemoryStream ms = new MemoryStream();
            FileStream fs;
            HSSFWorkbook templateWorkbook;
            ISheet sheet;
            IRow dataRow;
            int colum = 1;
            int fila = 6;
            String ruta = "\\Reportes\\";
            String file = "RpteDsctoConductores.xls";
            String hoja = "Hoja1";

            try
            {

                servicioTanqueo = new ServicioTanqueo();
                //obj = builder.RecorridoDetalle(p_obj);
                objTanDto = Mapper.Map<tanqueo, tanqueoDto>(p_obj);
                objTanDto = servicioTanqueo.listar(objTanDto);
                objTan = Mapper.Map<tanqueoDto, tanqueo>(objTanDto);
                                

                /*---------------------------------------------EXPORTACION -------------------------------------*/
                fs = new FileStream(Server.MapPath("~") + ruta + file, FileMode.Open, FileAccess.Read);
                templateWorkbook = new HSSFWorkbook(fs, true);
                sheet = templateWorkbook.GetSheet(hoja);

                int index = 0;
                foreach (tanqueo item in objTan.lstTan)
                {
                    colum = 1;
                    dataRow = sheet.GetRow(fila); if (dataRow.esNulo()) dataRow = sheet.CreateRow(fila); fila++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(++index); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(Convert.ToString(item.fecha).Substring(0, 10) + " " + item.hora + ":" + item.minuto); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.unidad_transporte.placa); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.unidad_transporte.conductor.nombres_apellidos); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.ruta.nombre); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue(item.serie_numero); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.nro_galones_abast); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.precio); colum++;
                    if (dataRow.GetCell(colum).esNulo()) dataRow.CreateCell(colum); dataRow.GetCell(colum).SetCellValue((Double)item.total); colum++;
                    


                }

                sheet.ForceFormulaRecalculation = true;
                templateWorkbook.Write(ms);
                TempData["Message"] = "Excel report created successfully!";
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {

            }
            return File(ms.ToArray(), "application/vnd.ms-excel", file);

        }



    }
}

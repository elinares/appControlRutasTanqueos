﻿using System.Web;
using System.Web.Optimization;

namespace appControlRutasTanqueos
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/busqueda_rutas.js",
                        "~/Scripts/busqueda_personas.js",  
                        "~/Scripts/jquery-{version}.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      /*"~/Content/bootstrap.css"
                      , "~/Content/site.css"  */                    
                      "~/Content/wrapper.css"
                      , "~/Content/estilos.css"
                      ));

            /*
            bundles.Add(new StyleBundle("~/Openpopup/lib").Include(
                      "~/Scripts/funciones.js" 
                      ,"~/Openpopup/lib/x.js"
                      , "~/Openpopup/lib/xfenster.js"
                      , "~/Openpopup/lib/xenabledrag.js"
                      , "~/Openpopup/lib/xdocsize.js"
                      , "~/Openpopup/lib/xname.js"
                      , "~/Openpopup/lib/popup.js"
                      , "~/Openpopup/Css/xfenster3.css"                      
                      ));
            */


        }
    }
}

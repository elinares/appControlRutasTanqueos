﻿using appControlRutasTanqueos.Areas.Privada.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using appControlRutasTanqueos.Servicios.Logica.Dtos;
using AutoMapper;

namespace appControlRutasTanqueos.Mapeos
{
    public class AutoMapperConfiguracion
    {

        public static void Configuracion()
        {


            Mapper.CreateMap<distrito, distritoDto>();
            Mapper.CreateMap<distritoDto, distrito>();

            Mapper.CreateMap<usuarioDto, usuario>();
            Mapper.CreateMap<usuario, usuarioDto>();

            Mapper.CreateMap<interfazDto, interfaz>();
            Mapper.CreateMap<interfaz, interfazDto>();

            Mapper.CreateMap<ruta, rutaDto>();
            Mapper.CreateMap<rutaDto, ruta>();

            Mapper.CreateMap<rendimiento, rendimientoDto>();
            Mapper.CreateMap<rendimientoDto,rendimiento>();

            Mapper.CreateMap<recorrido, recorridoDto>();
            Mapper.CreateMap<recorridoDto, recorrido>();

            Mapper.CreateMap<recorridoDetalle, recorridoDetalleDto>();
            Mapper.CreateMap<recorridoDetalleDto, recorridoDetalle>();


            Mapper.CreateMap<tanqueo, tanqueoDto>();
            Mapper.CreateMap<tanqueoDto, tanqueo>();

            Mapper.CreateMap<clase_documento, claseDocumentoDto>();
            Mapper.CreateMap<claseDocumentoDto, clase_documento>();

            Mapper.CreateMap<mercaderia, mercaderiaDto>();
            Mapper.CreateMap<mercaderiaDto, mercaderia>();


            Mapper.CreateMap<persona, personaDto>();
            Mapper.CreateMap<personaDto, persona>();

            Mapper.CreateMap<programacion, programacionDto>();
            Mapper.CreateMap<programacionDto, programacion>();

            Mapper.CreateMap<tipo_combustible, tipoCombustibleDto>();
            Mapper.CreateMap<tipoCombustibleDto, tipo_combustible>();

            Mapper.CreateMap<unidad_medida, unidadMedidaDto>();
            Mapper.CreateMap<unidadMedidaDto, unidad_medida>();

            Mapper.CreateMap<unidad_transporte, unidadTransporteDto>();
            Mapper.CreateMap<unidadTransporteDto, unidad_transporte>();

         

        }

    }
}
﻿using appControlRutasTanqueos.Areas.Privada.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Models
{
    internal class ViewModelBuilder
    {
        /* public int id_recorrido_detalle { get; set; }
        public recorridoDto recorrido { get; set; }
        public rutaDto trayectoria { get; set; } // Define los recorridos dentro de la ruta
        public List<String> lstAct { get; set; }//Lista de actividades: Descarga mercaderia origen, Descarga mercaderia en destino, Tanqueo en origen, Tanqueo en destino, etc                
        public tanqueoDto tanqueo { get; set; }
        public interfazDto interfaz { get; set; }
        public List<recorridoDetalleDto> lstTRD { get; set; }
         */
        internal recorridoViewModel RecorridoViewModel(recorrido p_obj)
        {


            var obj = new recorridoViewModel();
            obj.id_recorrido = p_obj.id_recorrido;
            obj.fecha = p_obj.fecha;
            obj.fecha_cadena = p_obj.fecha_cadena;

            obj.unidad_transporte.id_unidad_transporte = p_obj.unidad_transporte.id_unidad_transporte;
            obj.unidad_transporte.placa = p_obj.unidad_transporte.placa;
            obj.unidad_transporte.modelo = p_obj.unidad_transporte.modelo;
            obj.unidad_transporte.conductor.nombres_apellidos = p_obj.unidad_transporte.conductor.nombres_apellidos;
            obj.programacion = p_obj.programacion;
            obj.tanqueo = p_obj.tanqueo;
            obj.conductor = p_obj.conductor;
            obj.cliente = p_obj.cliente;
            obj.mercaderia = p_obj.mercaderia;            
            obj.ruta.id_ruta = p_obj.ruta.id_ruta;
            obj.ruta.nombre = p_obj.ruta.nombre;
            obj.ruta.ciudad_origen.id_distrito = p_obj.ruta.ciudad_origen.id_distrito;
            obj.ruta.ciudad_origen.nombre = p_obj.ruta.ciudad_origen.nombre;
            obj.ruta.ciudad_destino.id_distrito = p_obj.ruta.ciudad_destino.id_distrito;
            obj.ruta.ciudad_destino.nombre = p_obj.ruta.ciudad_destino.nombre;

            obj.interfaz.estado = p_obj.interfaz.estado;
            obj.interfaz.fecha_final = p_obj.interfaz.fecha_final;
            obj.interfaz.fecha_inicial = p_obj.interfaz.fecha_inicial;
            obj.interfaz.fecha_registro = p_obj.interfaz.fecha_registro;
            obj.interfaz.mensaje = p_obj.interfaz.mensaje;
            obj.interfaz.subtitulo = p_obj.interfaz.subtitulo;
            obj.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;
            obj.lstR = p_obj.lstR;
            obj.lstRD = p_obj.lstRD;
            obj.lstTan = p_obj.lstTan;
             
            return obj;
        }


        internal recorridoDetalleViewModel RecorridoDetalleViewModel( recorridoDetalle p_obj ) {

            var obj = new recorridoDetalleViewModel();
            obj.id_recorrido_detalle = p_obj.id_recorrido_detalle;
            obj.recorrido.id_recorrido = p_obj.recorrido.id_recorrido;
            obj.recorrido.programacion.id_programacion = p_obj.recorrido.programacion.id_programacion;
            obj.tanqueo.id_tanqueo = p_obj.tanqueo.id_tanqueo;
            obj.km_recor_esp = p_obj.km_recor_esp;
            obj.km_recor_real = p_obj.km_recor_real;
            obj.nro_galones_abast = p_obj.nro_galones_abast;
            obj.nro_galones_asig = p_obj.nro_galones_asig;
            obj.nro_galones_consum = p_obj.nro_galones_consum;
            obj.dif_km = p_obj.dif_km;
            obj.dif_glns = p_obj.dif_glns;
            obj.dif_rend = p_obj.dif_rend;

            obj.recorrido.unidad_transporte.id_unidad_transporte = p_obj.recorrido.unidad_transporte.id_unidad_transporte;
            obj.recorrido.unidad_transporte.placa = p_obj.recorrido.unidad_transporte.placa;
            obj.recorrido.unidad_transporte.modelo = p_obj.recorrido.unidad_transporte.modelo;
            obj.recorrido.unidad_transporte.conductor.nombres_apellidos = p_obj.recorrido.unidad_transporte.conductor.nombres_apellidos;

            obj.tanqueo.nro_galones_abast = p_obj.tanqueo.nro_galones_abast;            
            obj.tanqueo.kilometraje = p_obj.tanqueo.kilometraje;
            obj.tanqueo.rend_esperado = p_obj.tanqueo.rend_esperado;
            obj.tanqueo.rend_real = p_obj.tanqueo.rend_real;
            obj.tanqueo.fecha = p_obj.tanqueo.fecha;
            obj.tanqueo.precio = p_obj.tanqueo.precio;
            obj.tanqueo.total = p_obj.tanqueo.total;
            obj.tanqueo.proveedor.nombres_apellidos = p_obj.tanqueo.proveedor.nombres_apellidos;
            obj.tanqueo.distrito.id_distrito = p_obj.tanqueo.distrito.id_distrito;
            obj.tanqueo.distrito.nombre = p_obj.tanqueo.distrito.nombre;
            obj.tanqueo.tipo_tanqueo = p_obj.tanqueo.tipo_tanqueo;
            obj.tanqueo.tipo_combustible.nombre = p_obj.tanqueo.tipo_combustible.nombre;
            obj.recorrido.ruta.id_ruta = p_obj.recorrido.ruta.id_ruta;
            obj.recorrido.ruta.nombre = p_obj.recorrido.ruta.nombre;
            obj.recorrido.ruta.ciudad_origen.id_distrito = p_obj.recorrido.ruta.ciudad_origen.id_distrito;
            obj.recorrido.ruta.ciudad_origen.nombre = p_obj.recorrido.ruta.ciudad_origen.nombre;
            obj.recorrido.ruta.ciudad_destino.id_distrito = p_obj.recorrido.ruta.ciudad_destino.id_distrito;
            obj.recorrido.ruta.ciudad_destino.nombre = p_obj.recorrido.ruta.ciudad_destino.nombre;

            obj.recorrido.mercaderia.unidad_medida.id_unidad_medida = p_obj.recorrido.mercaderia.unidad_medida.id_unidad_medida;
            obj.recorrido.mercaderia.unidad_medida.abreviatura = p_obj.recorrido.mercaderia.unidad_medida.abreviatura;
            obj.recorrido.mercaderia.peso = p_obj.recorrido.mercaderia.peso;
            obj.recorrido.mercaderia.cliente.nombres_apellidos = p_obj.recorrido.mercaderia.cliente.nombres_apellidos;


            obj.trayectoria.id_ruta = p_obj.trayectoria.id_ruta;
            obj.trayectoria.nombre = p_obj.trayectoria.nombre;
            obj.tanqueo.serie_numero = p_obj.tanqueo.serie_numero;
            obj.tanqueo.rend_esperado = p_obj.tanqueo.rend_esperado;
            obj.tanqueo.rend_real =p_obj.tanqueo.rend_real;
            obj.tanqueo.nro_galones_asig = p_obj.tanqueo.nro_galones_asig;
            obj.lstRD = p_obj.lstRD;            
           
            obj.recorrido.interfaz.estado = p_obj.recorrido.interfaz.estado;
            obj.recorrido.interfaz.fecha_final = p_obj.recorrido.interfaz.fecha_final;
            obj.recorrido.interfaz.fecha_inicial = p_obj.recorrido.interfaz.fecha_inicial;
            obj.recorrido.interfaz.fecha_registro = p_obj.recorrido.interfaz.fecha_registro;
            obj.recorrido.interfaz.mensaje = p_obj.recorrido.interfaz.mensaje;
            obj.recorrido.interfaz.subtitulo = p_obj.recorrido.interfaz.subtitulo;
            obj.recorrido.interfaz.tipo_operacion = p_obj.recorrido.interfaz.tipo_operacion;
            obj.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;


            return obj;
        }

        internal  recorridoDetalle RecorridoDetalle(recorridoDetalleViewModel p_obj)
        {

            var obj = new recorridoDetalle();
            obj.id_recorrido_detalle = p_obj.id_recorrido_detalle;
            obj.recorrido.id_recorrido = p_obj.recorrido.id_recorrido;
            obj.recorrido.programacion.id_programacion = p_obj.recorrido.programacion.id_programacion;
            obj.tanqueo.id_tanqueo = p_obj.tanqueo.id_tanqueo;
            obj.km_recor_esp = p_obj.km_recor_esp;
            obj.km_recor_real = p_obj.km_recor_real;
            obj.nro_galones_abast = p_obj.nro_galones_abast;
            obj.nro_galones_asig = p_obj.nro_galones_asig;
            obj.dif_km=p_obj.dif_km;
            obj.dif_glns = p_obj.dif_glns;
            obj.dif_rend = p_obj.dif_rend;

            obj.recorrido.unidad_transporte.id_unidad_transporte = p_obj.recorrido.unidad_transporte.id_unidad_transporte;
            obj.recorrido.unidad_transporte.placa = p_obj.recorrido.unidad_transporte.placa;
            obj.recorrido.unidad_transporte.modelo = p_obj.recorrido.unidad_transporte.modelo;
            obj.recorrido.unidad_transporte.conductor.nombres_apellidos = p_obj.recorrido.unidad_transporte.conductor.nombres_apellidos;

            obj.tanqueo.nro_galones_abast = p_obj.tanqueo.nro_galones_abast;
            obj.tanqueo.kilometraje = p_obj.tanqueo.kilometraje;
            obj.tanqueo.fecha = p_obj.tanqueo.fecha;
            obj.tanqueo.precio = p_obj.tanqueo.precio;
            obj.tanqueo.total = p_obj.tanqueo.total;
            obj.tanqueo.proveedor.nombres_apellidos = p_obj.tanqueo.proveedor.nombres_apellidos;
            obj.tanqueo.distrito.id_distrito = p_obj.tanqueo.distrito.id_distrito;
            obj.tanqueo.distrito.nombre = p_obj.tanqueo.distrito.nombre;
            obj.tanqueo.tipo_tanqueo = p_obj.tanqueo.tipo_tanqueo;
            obj.tanqueo.tipo_combustible.nombre = p_obj.tanqueo.tipo_combustible.nombre;
            obj.tanqueo.rend_esperado = p_obj.tanqueo.rend_esperado;



            obj.recorrido.ruta.id_ruta = p_obj.recorrido.ruta.id_ruta;
            obj.recorrido.ruta.nombre = p_obj.recorrido.ruta.nombre;
            obj.recorrido.ruta.ciudad_origen.id_distrito = p_obj.recorrido.ruta.ciudad_origen.id_distrito;
            obj.recorrido.ruta.ciudad_origen.nombre = p_obj.recorrido.ruta.ciudad_origen.nombre;
            obj.recorrido.ruta.ciudad_destino.id_distrito = p_obj.recorrido.ruta.ciudad_destino.id_distrito;
            obj.recorrido.ruta.ciudad_destino.nombre = p_obj.recorrido.ruta.ciudad_destino.nombre;

            obj.recorrido.mercaderia.unidad_medida.id_unidad_medida = p_obj.recorrido.mercaderia.unidad_medida.id_unidad_medida;
            obj.recorrido.mercaderia.unidad_medida.abreviatura = p_obj.recorrido.mercaderia.unidad_medida.abreviatura;
            obj.recorrido.mercaderia.peso = p_obj.recorrido.mercaderia.peso;
            obj.recorrido.mercaderia.cliente.nombres_apellidos = p_obj.recorrido.mercaderia.cliente.nombres_apellidos;


            obj.trayectoria.id_ruta = p_obj.trayectoria.id_ruta;
            obj.trayectoria.nombre = p_obj.trayectoria.nombre;
            obj.tanqueo.serie_numero = p_obj.tanqueo.serie_numero;
            obj.tanqueo.rend_esperado = p_obj.tanqueo.rend_esperado;
            obj.tanqueo.rend_real = p_obj.tanqueo.rend_real;
            obj.tanqueo.nro_galones_asig = p_obj.tanqueo.nro_galones_asig;
            obj.lstRD = p_obj.lstRD;

            obj.recorrido.interfaz.estado = p_obj.recorrido.interfaz.estado;
            obj.recorrido.interfaz.fecha_final = p_obj.recorrido.interfaz.fecha_final;
            obj.recorrido.interfaz.fecha_inicial = p_obj.recorrido.interfaz.fecha_inicial;
            obj.recorrido.interfaz.fecha_registro = p_obj.recorrido.interfaz.fecha_registro;
            obj.recorrido.interfaz.mensaje = p_obj.recorrido.interfaz.mensaje;
            obj.recorrido.interfaz.subtitulo = p_obj.recorrido.interfaz.subtitulo;
            obj.recorrido.interfaz.tipo_operacion = p_obj.recorrido.interfaz.tipo_operacion;


            return obj;
        }

        internal rendimientoViewModel RendimientoViewModel(rendimiento p_obj)
        {

            var obj = new rendimientoViewModel();
            obj.id_rendimiento = p_obj.id_rendimiento;
            obj.ruta.id_ruta = p_obj.ruta.id_ruta;
            obj.ruta.ciudad_destino.id_distrito = p_obj.ruta.ciudad_destino.id_distrito;
            obj.ruta.ciudad_destino.nombre = p_obj.ruta.ciudad_destino.nombre;
            obj.ruta.ciudad_origen.id_distrito = p_obj.ruta.ciudad_origen.id_distrito;
            obj.ruta.ciudad_origen.nombre = p_obj.ruta.ciudad_origen.nombre;
            obj.ruta.nombre = p_obj.ruta.ciudad_origen.nombre + "-" + p_obj.ruta.ciudad_destino.nombre;
            obj.peso_maximo = p_obj.peso_maximo;
            obj.peso_minimo = p_obj.peso_minimo;
            obj.rend = p_obj.rend;

            obj.interfaz.estado = p_obj.interfaz.estado;
            obj.interfaz.fecha_final = p_obj.interfaz.fecha_final;
            obj.interfaz.fecha_inicial = p_obj.interfaz.fecha_inicial;
            obj.interfaz.fecha_registro = p_obj.interfaz.fecha_registro;
            obj.interfaz.mensaje = p_obj.interfaz.mensaje;
            obj.interfaz.subtitulo = p_obj.interfaz.subtitulo;
            obj.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;

            obj.lstRen = p_obj.lstRen;
            obj.ruta.lstRut = p_obj.ruta.lstRut;
            return obj;

        }


        internal rendimiento Rendimiento(rendimientoViewModel p_obj)
        {

            var obj = new rendimiento();
            obj.id_rendimiento = p_obj.id_rendimiento;
            obj.ruta.id_ruta = p_obj.ruta.id_ruta;
            obj.ruta.ciudad_destino.id_distrito = p_obj.ruta.ciudad_destino.id_distrito;
            obj.ruta.ciudad_destino.nombre = p_obj.ruta.ciudad_destino.nombre;
            obj.ruta.ciudad_origen.id_distrito = p_obj.ruta.ciudad_origen.id_distrito;
            obj.ruta.ciudad_origen.nombre = p_obj.ruta.ciudad_origen.nombre;
            obj.ruta.nombre = p_obj.ruta.ciudad_origen.nombre + "-" + p_obj.ruta.ciudad_destino.nombre;
            obj.peso_maximo = p_obj.peso_maximo;
            obj.peso_minimo = p_obj.peso_minimo;
            obj.rend = p_obj.rend;
            obj.interfaz.estado = p_obj.interfaz.estado;
            obj.interfaz.fecha_final = p_obj.interfaz.fecha_final;
            obj.interfaz.fecha_inicial = p_obj.interfaz.fecha_inicial;
            obj.interfaz.fecha_registro = p_obj.interfaz.fecha_registro;
            obj.interfaz.mensaje = p_obj.interfaz.mensaje;
            obj.interfaz.subtitulo = p_obj.interfaz.subtitulo;
            obj.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;
            obj.lstRen = p_obj.lstRen;
            obj.ruta.lstRut = p_obj.ruta.lstRut;
            return obj;

        }


        internal rutaViewModel RutaViewModel(ruta p_obj)
        {

            var obj = new rutaViewModel();
            obj.id_ruta = p_obj.id_ruta;
            obj.ciudad_destino.id_distrito = p_obj.ciudad_destino.id_distrito;
            obj.ciudad_destino.nombre = p_obj.ciudad_destino.nombre;
            obj.ciudad_origen.id_distrito = p_obj.ciudad_origen.id_distrito;
            obj.ciudad_origen.nombre = p_obj.ciudad_origen.nombre;
            obj.nombre = p_obj.ciudad_origen.nombre + "-" + p_obj.ciudad_destino.nombre;

            obj.interfaz.estado = p_obj.interfaz.estado;
            obj.interfaz.fecha_final = p_obj.interfaz.fecha_final;
            obj.interfaz.fecha_inicial = p_obj.interfaz.fecha_inicial;
            obj.interfaz.fecha_registro = p_obj.interfaz.fecha_registro;
            obj.interfaz.mensaje = p_obj.interfaz.mensaje;
            obj.interfaz.subtitulo = p_obj.interfaz.subtitulo;
            obj.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;

            obj.lstRut = p_obj.lstRut;            
            return obj;

        }

        
        internal tanqueoViewModel TanqueoViewModel(tanqueo p_obj)
        {

            var obj = new tanqueoViewModel();
            obj.id_tanqueo = p_obj.id_tanqueo;
            obj.id_recorrido_detalle = p_obj.id_recorrido_detalle;
            obj.serie = p_obj.serie;
            obj.numero = p_obj.numero;
            obj.serie_numero = p_obj.serie_numero;
            obj.fecha = p_obj.fecha;
            obj.hora = p_obj.hora;
            obj.minuto = p_obj.minuto;            

            obj.id_tanqueo = p_obj.id_tanqueo;
            obj.kilometraje = p_obj.kilometraje;            
            obj.kilometraje_anterior = p_obj.kilometraje_anterior;
            obj.nro_galones_abast = p_obj.nro_galones_abast;
            obj.nro_galones_asig = p_obj.nro_galones_asig;
            obj.nro_galones_anterior = p_obj.nro_galones_anterior;
            obj.precio = p_obj.precio;
            obj.total = p_obj.total;
            obj.rend_esperado = p_obj.rend_esperado;
            obj.rend_real = p_obj.rend_real;
            obj.tipo_tanqueo = p_obj.tipo_tanqueo;
            obj.clase_documento = p_obj.clase_documento;
            obj.proveedor = p_obj.proveedor;
            obj.tipo_combustible = p_obj.tipo_combustible;

            obj.unidad_transporte = p_obj.unidad_transporte;
            obj.empresa = p_obj.empresa;
            obj.ruta = p_obj.ruta;
            obj.ruta_retorno = p_obj.ruta_retorno;
            obj.glosa = p_obj.glosa;
            obj.detalle_ruta = p_obj.detalle_ruta;
            obj.ind_ficticio = p_obj.ind_ficticio;
            obj.ind_nodesemb = p_obj.ind_nodesemb;

            /*
            obj.distrito = p_obj.distrito;
            obj.ciudad_destino.id_distrito = p_obj.ciudad_destino.id_distrito;
            obj.ciudad_destino.nombre = p_obj.ciudad_destino.nombre;
            obj.ciudad_origen.id_distrito = p_obj.ciudad_origen.id_distrito;
            obj.ciudad_origen.nombre = p_obj.ciudad_origen.nombre;
            obj.nombre = p_obj.ciudad_origen.nombre + "-" + p_obj.ciudad_destino.nombre;*/

            obj.interfaz.estado = p_obj.interfaz.estado;
            obj.interfaz.fecha_final = p_obj.interfaz.fecha_final;
            obj.interfaz.fecha_inicial = p_obj.interfaz.fecha_inicial;
            obj.interfaz.fecha_registro = p_obj.interfaz.fecha_registro;
            obj.interfaz.mensaje = p_obj.interfaz.mensaje;
            obj.interfaz.subtitulo = p_obj.interfaz.subtitulo;
            obj.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;
            obj.lstTan = p_obj.lstTan;

            return obj;

        }

       
        /*internal distritoViewModel DistritoViewModel(distrito obj) {

            var objVM = new distritoViewModel();
            objVM.id_distrito = obj.id_distrito;
            objVM.nombre = obj.nombre;
            return objVM;
        }

        internal rutaViewModel RutaViewModel(ruta obj) {

            var objVM = new rutaViewModel();
            objVM.id_ruta = obj.id_ruta;
            objVM.ciudad_destino.id_distrito = obj.ciudad_destino.id_distrito;
            objVM.ciudad_destino.nombre = obj.ciudad_destino.nombre;
            objVM.ciudad_origen.id_distrito = obj.ciudad_origen.id_distrito;
            objVM.ciudad_origen.nombre = obj.ciudad_origen.nombre;
            return objVM;
        }*/

    }
}
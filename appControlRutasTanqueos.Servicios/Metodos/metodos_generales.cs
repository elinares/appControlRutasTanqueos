﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios
{
    public static class metodos_generales
    {
        public static bool esNulo(this object objeto)
        {
            return (objeto == null);
        }

        public static DateTime obtenerFecha(String formato)
        {
            DateTime fecha = new DateTime();

            if (formato == "actual")
                fecha = Convert.ToDateTime(Convert.ToString(DateTime.Now.Day) + "-" + Convert.ToString(DateTime.Now.Month) + "-" + Convert.ToString(DateTime.Now.Year));

            return fecha;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appControlRutasTanqueos.Servicios.Excepciones
{
    public class ErrorEnRendimiento : Exception
    {

        public ErrorEnRendimiento(string mensaje)
            : base(mensaje)
        {


        }

    }
}

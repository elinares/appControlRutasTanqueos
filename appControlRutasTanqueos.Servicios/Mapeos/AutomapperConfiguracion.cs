﻿using appControlRutasTanqueos.Repositorio.Dominio;
using appControlRutasTanqueos.Servicios.Logica.Dtos;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appControlRutasTanqueos.Servicios.Mapeos
{
    public class ServiciosAutoMapperConfiguracion
    {
        public static void Configuracion()
        {
            Mapper.CreateMap<distritoDto, distrito>();
            Mapper.CreateMap<distrito, distritoDto>();

            Mapper.CreateMap<usuarioDto, usuario>();
            Mapper.CreateMap<usuario, usuarioDto>();

            Mapper.CreateMap<interfazDto, interfaz>();
            Mapper.CreateMap<interfaz, interfazDto>();

            Mapper.CreateMap<ruta, rutaDto>();
            Mapper.CreateMap<rutaDto, ruta>();
            
            Mapper.CreateMap<rendimientoDto, rendimiento>();
            Mapper.CreateMap<rendimiento, rendimientoDto>();

       
            Mapper.CreateMap<recorrido, recorridoDto>();
            Mapper.CreateMap<recorridoDto, recorrido>();

            Mapper.CreateMap<recorridoDetalle, recorridoDetalleDto>();
            Mapper.CreateMap<recorridoDetalleDto, recorridoDetalle>();


            Mapper.CreateMap<tanqueo, tanqueoDto>();
            Mapper.CreateMap<tanqueoDto, tanqueo>();

            Mapper.CreateMap<claseDocumento, claseDocumentoDto>();
            Mapper.CreateMap<claseDocumentoDto, claseDocumento>();

            Mapper.CreateMap<mercaderia, mercaderiaDto>();
            Mapper.CreateMap<mercaderiaDto, mercaderia>();


            Mapper.CreateMap<persona, personaDto>();
            Mapper.CreateMap<personaDto, persona>();

            Mapper.CreateMap<programacion, programacionDto>();
            Mapper.CreateMap<programacionDto, programacion>();

            Mapper.CreateMap<tipoCombustible, tipoCombustibleDto>();
            Mapper.CreateMap<tipoCombustibleDto, tipoCombustible>();

            Mapper.CreateMap<unidadMedida, unidadMedidaDto>();
            Mapper.CreateMap<unidadMedidaDto, unidadMedida>();

            Mapper.CreateMap<unidadTransporte, unidadTransporteDto>();
            Mapper.CreateMap<unidadTransporteDto, unidadTransporte>();
           
        }
    }
}

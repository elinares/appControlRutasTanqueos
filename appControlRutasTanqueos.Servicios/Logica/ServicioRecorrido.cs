﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using appControlRutasTanqueos.Servicios.Excepciones;
using System.Data;
using appControlRutasTanqueos.Servicios.Logica.Dtos;
using appControlRutasTanqueos.Repositorio.Impl;
using AutoMapper;
using appControlRutasTanqueos.Repositorio.Dominio;
using appControlRutasTanqueos.Repositorio.EF;
using System.Data.Common;

namespace appControlRutasTanqueos.Servicios.Logica
{
    public class ServicioRecorrido
    {
        public static ConexionGenerica con = null;
        public static DbTransaction t = null;
        private readonly RepositorioRecorrido repositorioRecorrido;
        private readonly RepositorioRecorridoDetalle repositorioRecorridoDetalle;
        private readonly RepositorioTanqueo repositorioTanqueo;

        public ServicioRecorrido()
        {
            con = new ConexionGenerica();
            repositorioRecorrido = new RepositorioRecorrido(new VOfficeDbContext("name=VOffice"));
            repositorioRecorridoDetalle = new RepositorioRecorridoDetalle(new VOfficeDbContext("name=VOffice"));
            repositorioTanqueo = new RepositorioTanqueo(new VOfficeDbContext("name=VOffice"));
        }

        public recorridoDto generar_iae(recorridoDto p_obj)
        {

            try
            {               
                t = con.obtenerConexion().BeginTransaction();
                var obj = Mapper.Map<recorridoDto, recorrido>(p_obj);
                obj=repositorioRecorrido.generar_iae(obj, con);
                foreach (var item in obj.lstRD)
                {
                    if (item.id_recorrido_detalle==0) item.interfaz.tipo_operacion = "I";
                    else item.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;
                    item.recorrido = obj;
                    repositorioRecorridoDetalle.generar_iae(item, con);
                }
                foreach (var item in obj.lstTan)
                {                    
                    if (item.id_tanqueo == 0) item.interfaz.tipo_operacion = "I";
                    else item.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;
                    repositorioTanqueo.generar_iae(item, con);
                }

                p_obj = Mapper.Map<recorrido, recorridoDto>(obj);
                t.Commit();

            }
            catch (Exception e)
            {

                t.Rollback();
                throw (new Exception(e.Message));
            }
            finally
            {
                 if (!con.obtenerConexion().esNulo()) 
                 if (con.obtenerConexion().State == ConnectionState.Open) 
                     con.obtenerConexion().Close();
            }

            return p_obj;

        }


        public recorridoDto obtener(recorridoDto p_obj)
        {

            try
            {
                var obj = Mapper.Map<recorridoDto, recorrido>(p_obj);
                obj = repositorioRecorrido.obtener(obj, con);
                p_obj = Mapper.Map<recorrido, recorridoDto>(obj);

            }
            catch (Exception e)
            {
                //throw new ErrorEnRendimiento("Hubo un error al procesar los datos.");
                throw new ErrorEnRendimiento(e.Message);
            }
            finally
            {
                if (!con.obtenerConexion().esNulo())
                    if (con.obtenerConexion().State == ConnectionState.Open)
                        con.obtenerConexion().Close();

            }

            return p_obj;

        }


        public recorridoDto obtenerJson(recorridoDto p_obj)
        {

            try
            {
                var obj = Mapper.Map<recorridoDto, recorrido>(p_obj);
                obj = repositorioRecorrido.obtenerJson(obj, con);
                p_obj = Mapper.Map<recorrido, recorridoDto>(obj);

            }
            catch (Exception e)
            {
                //throw new ErrorEnRendimiento("Hubo un error al procesar los datos.");
                throw new ErrorEnRendimiento(e.Message);
            }
            finally
            {
                if (!con.obtenerConexion().esNulo())
                    if (con.obtenerConexion().State == ConnectionState.Open)
                        con.obtenerConexion().Close();

            }

            return p_obj;

        }

        public recorridoDto obtenerConDetalle(recorridoDto p_obj)
        {

            try
            {
                var obj = Mapper.Map<recorridoDto, recorrido>(p_obj);
                obj.interfaz.fecha_inicial = Convert.ToDateTime("01-01-0001");
                obj = repositorioRecorrido.obtener(obj, con);                

                if (obj.id_recorrido == -1 || obj.id_recorrido == 0)
                {
                    obj.lstRD = new List<recorridoDetalle>();
                    obj.lstRD.Add(new recorridoDetalle());
                    obj.lstTan.Add(new tanqueo());
                }
                else {

                    var objRD = new recorridoDetalle();
                    objRD.recorrido = obj;
                    obj.lstRD = repositorioRecorridoDetalle.obtener(objRD, con).lstRD;

                    var objTan = new tanqueo();
                    objTan.id_programacion = obj.programacion.id_programacion;
                    
                    obj.lstTan = repositorioTanqueo.obtener(objTan, con).lstTan;

                }
                
                p_obj = Mapper.Map<recorrido, recorridoDto>(obj);

            }
            catch (Exception e)
            {
                //throw new ErrorEnRendimiento("Hubo un error al procesar los datos.");
                throw new ErrorEnRendimiento(e.Message);
            }
            finally
            {
                if (!con.obtenerConexion().esNulo())
                    if (con.obtenerConexion().State == ConnectionState.Open)
                        con.obtenerConexion().Close();

            }

            return p_obj;

        }

        
    }

}

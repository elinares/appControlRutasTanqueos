﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using appControlRutasTanqueos.Servicios.Excepciones;
using System.Data;
using appControlRutasTanqueos.Servicios.Logica.Dtos;
using appControlRutasTanqueos.Repositorio.Impl;
using appControlRutasTanqueos.Repositorio.EF;
using appControlRutasTanqueos.Repositorio.Dominio;
using AutoMapper;
using System.Data.Common;

namespace appControlRutasTanqueos.Servicios.Logica
{
    public class ServicioRendimiento
    {
        public static ConexionGenerica con = null;
        public static DbTransaction t = null;        
        private readonly RepositorioRendimiento repositorioRendimiento;
        private readonly RepositorioRuta repositorioRuta;

        public ServicioRendimiento()
        {
            con = new ConexionGenerica();
            //Inversion de COntrol, IInyeccion de Dependencia
            repositorioRendimiento = new RepositorioRendimiento(new VOfficeDbContext("name=VOffice"));
            repositorioRuta = new RepositorioRuta(new VOfficeDbContext("name=VOffice"));
            
        }

        public rendimientoDto generar_iae(rendimientoDto p_obj)
        {
            
            try
            {                
                var obj = Mapper.Map<rendimientoDto, rendimiento>(p_obj);
                t = con.obtenerConexion().BeginTransaction();
                foreach (var item in obj.lstRen)
                {
                   item.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;
                   obj = repositorioRendimiento.generar_iae(item,con);
                }
                p_obj = Mapper.Map<rendimiento, rendimientoDto>(obj);
                t.Commit();

            }
            catch (Exception e)
            {

                t.Rollback();
                throw (new Exception(e.Message));
            }
            finally
            {

               /* if (!con.obtenerConexion().esNulo()) 
                    if (con.obtenerConexion().State == ConnectionState.Open) 
                        con.obtenerConexion().Close();*/

            }

            return p_obj;

        }


        public rendimientoDto listar(rendimientoDto p_obj)
        {

            try
            {                
                var obj = Mapper.Map<rendimientoDto, rendimiento>(p_obj);
                obj = repositorioRendimiento.listar(obj, con);
                p_obj = Mapper.Map<rendimiento, rendimientoDto>(obj);
            }
            catch (Exception e)
            {
                //throw new ErrorEnRendimiento("Hubo un error al procesar los datos.");
                throw new ErrorEnRendimiento(e.Message);
            }
            finally
            {
                if (!con.obtenerConexion().esNulo()) 
                  if (con.obtenerConexion().State == ConnectionState.Open) 
                      con.obtenerConexion().Close();
            }

            return p_obj;

        }


        public rendimientoDto obtener(rendimientoDto p_obj)
        {
            ruta objRut = new ruta();
            try
            {
                
               var  obj = Mapper.Map<rendimientoDto, rendimiento>(p_obj);
               obj = repositorioRendimiento.obtener(obj, con);
               obj.ruta.lstRut = repositorioRuta.listar(objRut,con).lstRut;
               p_obj = Mapper.Map<rendimiento, rendimientoDto>(obj);               

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {
                if (!con.obtenerConexion().esNulo()) 
                    if (con.obtenerConexion().State == ConnectionState.Open) 
                        con.obtenerConexion().Close();
            }
            return p_obj;

        }


    }
}

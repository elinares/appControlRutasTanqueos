﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using appControlRutasTanqueos.Servicios.Excepciones;
using System.Data;
using appControlRutasTanqueos.Servicios.Logica.Dtos;
using appControlRutasTanqueos.Repositorio.Impl;
using appControlRutasTanqueos.Repositorio.EF;
using appControlRutasTanqueos.Repositorio.Dominio;
using AutoMapper;
using System.Data.Common;

namespace appControlRutasTanqueos.Servicios.Logica
{
    public class ServicioTanqueo
    {
        public static ConexionGenerica con = null;
        public static DbTransaction t = null;        
        private readonly RepositorioTanqueo repositorioTanqueo;        

        public ServicioTanqueo()
        {
            con = new ConexionGenerica();
            //Inversion de COntrol, IInyeccion de Dependencia
            repositorioTanqueo = new RepositorioTanqueo(new VOfficeDbContext("name=VOffice"));
            
        }

        public tanqueoDto generar_iae(tanqueoDto p_obj)
        {
            
            try
            {
                var obj = Mapper.Map<tanqueoDto, tanqueo>(p_obj);
                t = con.obtenerConexion().BeginTransaction();
                foreach (var item in obj.lstTan)
                {
                   item.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;
                   obj = repositorioTanqueo.generar_iae(item, con);
                }
                p_obj = Mapper.Map<tanqueo, tanqueoDto>(obj);
                t.Commit();

            }
            catch (Exception e)
            {

                t.Rollback();
                throw (new Exception(e.Message));
            }
            finally
            {

               /* if (!con.obtenerConexion().esNulo()) 
                    if (con.obtenerConexion().State == ConnectionState.Open) 
                        con.obtenerConexion().Close();*/

            }

            return p_obj;

        }


        public tanqueoDto listar(tanqueoDto p_obj)
        {

            try
            {
                var obj = Mapper.Map<tanqueoDto, tanqueo>(p_obj);
                obj = repositorioTanqueo.listar(obj, con);
                p_obj = Mapper.Map<tanqueo, tanqueoDto>(obj);
            }
            catch (Exception e)
            {
                //throw new ErrorEnRendimiento("Hubo un error al procesar los datos.");
                throw new ErrorEnRendimiento(e.Message);
            }
            finally
            {
                if (!con.obtenerConexion().esNulo()) 
                  if (con.obtenerConexion().State == ConnectionState.Open) 
                      con.obtenerConexion().Close();
            }
            return p_obj;
        }


        public tanqueoDto obtener(tanqueoDto p_obj)
        {
            tanqueo objTan = new tanqueo();
            try
            {

               var obj = Mapper.Map<tanqueoDto, tanqueo>(p_obj);
               obj = repositorioTanqueo.obtener(obj, con);
               obj.lstTan = repositorioTanqueo.listar(objTan, con).lstTan;
               p_obj = Mapper.Map<tanqueo, tanqueoDto>(obj);               

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {
                if (!con.obtenerConexion().esNulo()) 
                    if (con.obtenerConexion().State == ConnectionState.Open) 
                        con.obtenerConexion().Close();
            }
            return p_obj;

        }


    }
}

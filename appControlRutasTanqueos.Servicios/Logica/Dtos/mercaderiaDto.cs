﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class mercaderiaDto
    {
        public int id_mercaderia { get; set; }
        public string nombre { get; set; }
        public decimal peso { get; set; }
        public unidadMedidaDto unidad_medida { get; set; }
        public personaDto cliente { set; get; }

        public mercaderiaDto() {
            this.unidad_medida = new unidadMedidaDto();
            this.cliente = new personaDto();
            this.peso = 0;
        }


    }
}
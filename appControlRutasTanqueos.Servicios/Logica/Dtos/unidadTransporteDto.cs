﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class unidadTransporteDto
    {
        public int id_unidad_transporte { get; set; }
        public string placa { get; set; }
        public string modelo { get; set; }
        public personaDto conductor { get; set; }

        public unidadTransporteDto()
        {
            this.id_unidad_transporte = 0;
            this.conductor = new personaDto();

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class usuarioDto
    {
        public int id_usuario { get; set; }
        public string nombre { get; set; }

        public usuarioDto()
        {
            this.id_usuario = 0;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class rendimientoDto
    {
        public int id_rendimiento { get; set; }
        public rutaDto ruta { get; set; }
        public decimal peso_minimo { get; set; }
        public decimal peso_maximo { get; set; }
        public decimal rend { get; set; } //Rendimiento
        public interfazDto interfaz { get; set; }
        public List<rendimientoDto> lstRen { get; set; }

        public rendimientoDto()
        {
            this.id_rendimiento = 0;
           this.ruta = new rutaDto();
            this.peso_minimo = 0;
            this.peso_maximo = 0;
            this.rend = 0;
           this.interfaz = new interfazDto();
            this.lstRen = new List<rendimientoDto>();
        }


    }


}

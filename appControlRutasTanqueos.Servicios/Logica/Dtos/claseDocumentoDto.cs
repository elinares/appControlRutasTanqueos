﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class claseDocumentoDto
    {
        public int id_clase_documento { get; set; }
        public string nombre { get; set; }

        public claseDocumentoDto()
        {
            this.id_clase_documento = 0;
        }
    }
}
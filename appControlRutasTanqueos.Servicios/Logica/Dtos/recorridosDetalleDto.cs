﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class recorridoDetalleDto
    {
        public int id_recorrido_detalle { get; set; }
        public recorridoDto recorrido { get; set; }
        public rutaDto trayectoria { get; set; } // Define los recorridos dentro de la ruta
        public Decimal km_recor_esp { get; set; }
        public Decimal km_recor_real { get; set; }
        public Decimal nro_galones_abast { get; set; }//Almacena el total de galones abastecidos en un recorrido con uno o mas tanqueos
        public Decimal nro_galones_asig { get; set; }//Almacena el total de galones asignados en un recorrido con uno o mas tanqueos
        public Decimal nro_galones_consum { get; set; }
        public Decimal dif_km { get; set; }
        public Decimal dif_glns { get; set; }
        public Decimal dif_rend { get; set; }

        public List<String> lstAct { get; set; }//Lista de actividades: Descarga mercaderia origen, Descarga mercaderia en destino, Tanqueo en origen, Tanqueo en destino, etc                
        public tanqueoDto tanqueo { get; set; }
        public interfazDto interfaz { get; set; }
        public List<recorridoDetalleDto> lstRD { get; set; }


        public recorridoDetalleDto()
        {

            this.id_recorrido_detalle = 0;
            this.recorrido = new recorridoDto();
            this.trayectoria = new rutaDto();
            this.km_recor_esp = 0;
            this.km_recor_real = 0;
            this.nro_galones_abast = 0;
            this.nro_galones_asig = 0;
            this.tanqueo = new tanqueoDto();
            this.interfaz = new interfazDto();
            this.lstAct = new List<String>();
           

        }



    }
}
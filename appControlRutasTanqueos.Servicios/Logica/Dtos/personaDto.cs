﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class personaDto
    {
        public int id_persona { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string nombres_apellidos { get; set; }
        public Decimal tarifa_combustible { get; set; }

       public interfazDto interfaz { get; set; }
        public List<personaDto> lstPer { get; set; }

        public personaDto()
        {
            this.id_persona = 0;
            this.nombres_apellidos = "";
            this.nombres = "";
            this.interfaz = new interfazDto();

        }


    }
}
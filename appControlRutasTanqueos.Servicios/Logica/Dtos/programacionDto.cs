﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class programacionDto
    {
        public int id_programacion { get; set; }
        public usuarioDto usuario { get; set; }
        public DateTime fecha { get; set; }

        public programacionDto()
        {
            this.id_programacion = 0;
            this.usuario = new usuarioDto();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class distritoDto
    {
        public int id_distrito { get; set; }
        public string nombre { get; set; }

        public distritoDto()
        {
            this.id_distrito = 0;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
     public class recorridoDto
    {
        public int id_recorrido { get; set; }
        public DateTime fecha { get; set; }
        public String fecha_cadena { get; set; }
        public unidadTransporteDto unidad_transporte { get; set; }
        public programacionDto programacion { get; set; }
        public personaDto conductor { get; set; }
        public personaDto cliente { get; set; }
        public mercaderiaDto mercaderia { get; set; }
        public rutaDto ruta { get; set; } // Define la ruta de origen y destino
        public interfazDto interfaz { get; set; }
        public tanqueoDto tanqueo { get; set; }

         public List<recorridoDto> lstR { get; set; }
         public List<recorridoDetalleDto> lstRD { get; set; }
         public List<rutaDto> lstRut { get; set; }
         public List<tanqueoDto> lstTan { get; set; } 
         
        public recorridoDto()
        {

            this.id_recorrido = 0;
            this.fecha = DateTime.Now;
            this.interfaz = new interfazDto();
            this.unidad_transporte = new unidadTransporteDto();
            this.programacion = new programacionDto();
            this.conductor = new personaDto();
            this.cliente = new personaDto();
            this.tanqueo = new tanqueoDto();
            this.mercaderia = new mercaderiaDto();
            this.ruta = new rutaDto();
            this.interfaz=new interfazDto();
            this.lstR = new List<recorridoDto>();
            this.lstRD = new List<recorridoDetalleDto>();
         

        }

    }

}
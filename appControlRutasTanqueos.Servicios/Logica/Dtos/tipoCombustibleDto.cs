﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class tipoCombustibleDto
    {
        public int id_tipo_combustible { get; set; }
        public string nombre { get; set; }

        public tipoCombustibleDto()
        {

            this.id_tipo_combustible = 0;
            this.nombre = "PETR";
        }

    }
}
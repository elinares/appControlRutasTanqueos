﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class tanqueoDto
    {
       
        public int id_tanqueo { get; set; }
        /*--Ticket que genera el grifo al momento de provisonal de combustible la unidad--*/
        public claseDocumentoDto clase_documento { get; set; }
        public int id_recorrido_detalle { get; set; }      

        public string serie { get; set; }
        public string numero { get; set; }
        public string serie_numero { get; set; }
        public decimal nro_galones_anterior { get; set; }
        public decimal nro_galones_abast {get;set; }
        public decimal nro_galones_asig { get; set; }
        public decimal kilometraje_anterior { get; set; }
        public decimal kilometraje {get;set; } 
        public decimal rend_real { get; set; }// Rendimiento = Km. recorridos / nro_galones tanqueados  
        public decimal rend_esperado {get;set;}// Rendimiento = Km. recorridos / nro_galones tanqueados           
        public DateTime fecha { get; set; }
        public String hora { get; set; }
        public String minuto { get; set; }

        public decimal precio { get; set; }
        public decimal total { get; set; }
        public personaDto proveedor { get; set; }
        public distritoDto distrito { get; set; }//Ludar donde se tanqueo.
        public tipoCombustibleDto tipo_combustible { get; set; } //GASOLINA O PETROLEO
        public string tipo_tanqueo { get; set; } //FULL o PARCIAL

        public int id_programacion { get; set; }
        public unidadTransporteDto unidad_transporte { get; set; }
        public personaDto empresa { get; set; }
        public String glosa { get; set; }
        public interfazDto interfaz { get; set; }
        public Boolean ind_nodesemb { get; set; }
        public String detalle_ruta { get; set; }
        public rutaDto ruta { get; set; }
        public rutaDto ruta_retorno { get; set; }
        public Boolean ind_ficticio { get; set; }

        public List<tanqueoDto> lstTan { get; set; }

        public tanqueoDto()
        {

            this.id_tanqueo = 0;
            this.nro_galones_anterior = 0;
            this.kilometraje = 0;
            this.kilometraje_anterior = 0;
            this.nro_galones_abast = 0;
            this.nro_galones_asig = 0;
            this.precio = 0;
            this.rend_esperado = 0;
            this.rend_real = 0;
            this.fecha = DateTime.Now;
            this.hora = "00";
            this.minuto = "00";
            this.detalle_ruta = "";
            this.serie = "";
            this.numero = "";
            this.serie_numero = "";
            this.detalle_ruta = "";
            this.glosa = "";
            this.ind_ficticio = false;
            this.ind_nodesemb = false;
            this.tipo_tanqueo = "FUL";
            this.clase_documento = new claseDocumentoDto();
            this.proveedor = new personaDto();
            this.distrito = new distritoDto();
            this.tipo_combustible = new tipoCombustibleDto();
            this.interfaz = new interfazDto();

            this.unidad_transporte = new unidadTransporteDto();
            this.empresa = new personaDto();
            this.ruta = new rutaDto();
            this.ruta_retorno = new rutaDto();
            
        }

    }
}
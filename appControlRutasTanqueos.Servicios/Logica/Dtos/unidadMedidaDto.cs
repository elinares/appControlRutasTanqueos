﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class unidadMedidaDto
    {
        public int id_unidad_medida { get; set; }
        public string nombre { get; set; }
        public string abreviatura { get; set; }

        public unidadMedidaDto()
        {

            this.id_unidad_medida = 0;

        }
    }
}
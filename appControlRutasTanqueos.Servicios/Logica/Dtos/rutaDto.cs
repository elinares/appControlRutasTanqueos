﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Servicios.Logica.Dtos
{
    public class rutaDto
    {
        public int id_ruta { get; set; }
        public String nombre { get; set; }
        public distritoDto ciudad_origen { get; set; }
        public distritoDto ciudad_destino { get; set; }

        public decimal tiempo_aprox { get; set; }
        public decimal km_aprox { get; set; }
        public decimal rend_esperado { get; set; }
        public decimal peso { get; set; }
        public List<rutaDto> lstRut { get; set; }

        public interfazDto interfaz { get; set; }


        public rutaDto()
        {

            this.id_ruta = 0;
            this.tiempo_aprox = 0;
            this.km_aprox = 0;
            this.peso = 0;
            this.interfaz = new interfazDto();
            this.ciudad_destino = new distritoDto();
            this.ciudad_origen = new distritoDto();
        }


    }
}
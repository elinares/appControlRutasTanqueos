﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using appControlRutasTanqueos.Servicios.Excepciones;
using System.Data;
using appControlRutasTanqueos.Servicios.Logica.Dtos;
using appControlRutasTanqueos.Repositorio.Impl;
using AutoMapper;
using appControlRutasTanqueos.Repositorio.Dominio;
using appControlRutasTanqueos.Repositorio.EF;
using System.Data.Common;

namespace appControlRutasTanqueos.Servicios.Logica
{
    public class ServicioRuta
    {
        public static ConexionGenerica con = null;
        public static DbTransaction t = null;
        private readonly RepositorioRuta repositorioRuta;
        

        public ServicioRuta()
        {
            con = new ConexionGenerica();
            repositorioRuta = new RepositorioRuta(new VOfficeDbContext("name=VOffice"));
        }

        public rutaDto listar(rutaDto p_obj)
        {

            try
            {                
                var obj = Mapper.Map<rutaDto, ruta>(p_obj);
                obj = repositorioRuta.listar(obj, con);
                p_obj = Mapper.Map<ruta, rutaDto>(obj);

            }
            catch (Exception e)
            {
                //throw new ErrorEnRendimiento("Hubo un error al procesar los datos.");
                throw new ErrorEnRendimiento(e.Message);
            }
            finally
            {

               /* if (!con.obtenerConexion().esNulo())
                    if (con.obtenerConexion().State == ConnectionState.Open)
                        con.obtenerConexion().Close();*/
            }

            return p_obj;

        }

        public personaDto listar_persona(personaDto p_obj)
        {

            try
            {
                var obj = Mapper.Map<personaDto, persona>(p_obj);
                obj = repositorioRuta.listar_persona(obj, con);
                p_obj = Mapper.Map<persona, personaDto>(obj);

            }
            catch (Exception e)
            {
                throw new ErrorEnRendimiento(e.Message);
            }
            finally
            {

            }

            return p_obj;

        }
    }

}

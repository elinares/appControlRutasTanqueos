﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using appControlRutasTanqueos.Servicios.Excepciones;
using System.Data;
using appControlRutasTanqueos.Servicios.Logica.Dtos;
using appControlRutasTanqueos.Repositorio.Impl;
using AutoMapper;
using appControlRutasTanqueos.Repositorio.Dominio;
using appControlRutasTanqueos.Repositorio.EF;
using System.Data.Common;

namespace appControlRutasTanqueos.Servicios.Logica
{
    public class ServicioRecorridoDetalle
    {
        public static ConexionGenerica con = null;
        public static DbTransaction t = null;
        private readonly RepositorioRecorridoDetalle repositorioRecorridoDetalle;    

        public ServicioRecorridoDetalle()
        {
            con = new ConexionGenerica();
            repositorioRecorridoDetalle = new RepositorioRecorridoDetalle(new VOfficeDbContext("name=VOffice"));
        }

        public recorridoDetalleDto generar_iae(recorridoDetalleDto p_obj)
        {

            try
            {
                var obj = Mapper.Map<recorridoDetalleDto, recorridoDetalle>(p_obj);
                t = con.obtenerConexion().BeginTransaction();
                foreach (var item in obj.lstRD)
                {
                    item.interfaz.tipo_operacion = p_obj.interfaz.tipo_operacion;
                    obj = repositorioRecorridoDetalle.generar_iae(item, con);
                }
                p_obj = Mapper.Map<recorridoDetalle, recorridoDetalleDto>(obj);
                t.Commit();

            }
            catch (Exception e)
            {

                t.Rollback();
                throw (new Exception(e.Message));
            }
            finally
            {

            }

            return p_obj;

        }


        public recorridoDetalleDto obtener(recorridoDetalleDto p_obj)
        {

            try
            {                
                var obj = Mapper.Map<recorridoDetalleDto, recorridoDetalle>(p_obj);
                obj = repositorioRecorridoDetalle.obtener(obj, con);
                p_obj = Mapper.Map<recorridoDetalle, recorridoDetalleDto>(obj);

            }
            catch (Exception e)
            {
                //throw new ErrorEnRendimiento("Hubo un error al procesar los datos.");
                throw new ErrorEnRendimiento(e.Message);
            }
            finally
            {

            }

            return p_obj;

        }

        public recorridoDetalleDto listar_traza_recorrido_detalle(recorridoDetalleDto p_obj)
        {

            try
            {
                var obj = Mapper.Map<recorridoDetalleDto, recorridoDetalle>(p_obj);
                obj = repositorioRecorridoDetalle.listar_traza_recorrido_detalle(obj, con);
                p_obj = Mapper.Map<recorridoDetalle, recorridoDetalleDto>(obj);

            }
            catch (Exception e)
            {
                //throw new ErrorEnRendimiento("Hubo un error al procesar los datos.");
                throw new ErrorEnRendimiento(e.Message);
            }
            finally
            {

            }

            return p_obj;

        }
    }

}

﻿using appControlRutasTanqueos.Repositorio.Dominio;
using appControlRutasTanqueos.Repositorio.EF;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;

namespace appControlRutasTanqueos.Repositorio.Impl
{
    public class RepositorioRendimiento : RepositorioBase<rendimiento, VOfficeDbContext>
    {     
        public List<rendimiento> lst = null;
        private String name_sp;
        private DbCommand objetoCommand = null;
        private DbDataAdapter objetoDataAdapter = null;
        private DataSet objetoDataSet = null;
        private DataTable dt = null;

        public RepositorioRendimiento(VOfficeDbContext context)
            : base(context)
        {
            dt = new DataTable();
            lst = new List<rendimiento>();
        }


        public rendimiento generar_iae(rendimiento entidad, ConexionGenerica con)
        {
            
            try
            {
            
                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_rendimiento_iae";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;
            
                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter(); param.Direction = ParameterDirection.Output;
                param.ParameterName = "o_id_rendimiento"; param.DbType = DbType.Int32;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_rendimiento"; param.DbType = DbType.Int32;
                param.Value = entidad.id_rendimiento;objetoCommand.Parameters.Add(param); 
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta.id_ruta; objetoCommand.Parameters.Add(param); 
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_peso_minimo"; param.DbType = DbType.Decimal;
                param.Value = entidad.peso_minimo;objetoCommand.Parameters.Add(param); 
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_peso_maximo"; param.DbType = DbType.Decimal;
                param.Value = entidad.peso_maximo;objetoCommand.Parameters.Add(param); 
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_rend"; param.DbType = DbType.Decimal;
                param.Value = entidad.rend;objetoCommand.Parameters.Add(param); 
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_usuario"; param.DbType = DbType.Int32;
                param.Value = entidad.interfaz.usuario.id_usuario;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_registro"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_registro;objetoCommand.Parameters.Add(param); 
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado;objetoCommand.Parameters.Add(param);

                objetoCommand.ExecuteNonQuery();
                entidad.id_rendimiento = (Int32)objetoCommand.Parameters["o_id_rendimiento"].Value;


            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {
            }

            return entidad;
        }


        public rendimiento listar(rendimiento entidad, ConexionGenerica con)
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_rendimiento_listar";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter();                
                param.ParameterName = "i_tipo_operacion";param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_rendimiento"; param.DbType = DbType.Int32;
                param.Value = entidad.id_rendimiento;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta.id_ruta;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_usuario"; param.DbType = DbType.Int32; 
                param.Value = entidad.interfaz.usuario.id_usuario;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_registro"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_registro;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_inicial"; param.DbType = DbType.String; 
                param.Value = entidad.interfaz.fecha_inicial;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_final"; param.DbType = DbType.String; 
                param.Value = entidad.interfaz.fecha_final;objetoCommand.Parameters.Add(param);

                // Obtenemos  el DataAdapter siempre despues de fijar la consulta en Command
                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                // Adaptador enlazado a la consulta
                objetoDataAdapter.SelectCommand = objetoCommand;
                // Instanciamos el DataSet
                objetoDataSet = new DataSet();
                // Llenamos el DataSet con los datos obtenidos de la consulta
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];

                if (num_rows > 0)
                {
                    //num_rows = ds.Tables[0].Rows.Count;
                    num_rows = dt.Rows.Count;
                    for (int i = 0; i <= num_rows - 1; i++)
                    {
                        rendimiento obj = new rendimiento();

                        obj.id_rendimiento = (Int32)dt.Rows[i]["o_id_rendimiento"];
                        obj.ruta.id_ruta = (Int32)dt.Rows[i]["o_id_ruta"];
                        obj.ruta.ciudad_origen.nombre = (String)dt.Rows[i]["o_ruta_origen"];
                        obj.ruta.ciudad_destino.nombre = (String)dt.Rows[i]["o_ruta_destino"];
                        obj.ruta.nombre = obj.ruta.ciudad_origen.nombre + "-" + obj.ruta.ciudad_destino.nombre;
                        obj.peso_minimo = (Decimal)dt.Rows[i]["o_peso_minimo"];
                        obj.peso_maximo = (Decimal)dt.Rows[i]["o_peso_maximo"];
                        obj.rend = (Decimal)dt.Rows[i]["o_rend"];
                        obj.interfaz.usuario.id_usuario = (Int32)dt.Rows[i]["o_id_usuario"];
                        obj.interfaz.usuario.nombre = (String)dt.Rows[i]["o_usuario"];
                        obj.interfaz.fecha_registro = Convert.ToDateTime(dt.Rows[i]["o_fecha_registro"]);
                        obj.interfaz.estado = (Boolean)dt.Rows[i]["o_estado"];

                        lst.Add(obj);

                        //Console.WriteLine(obj.ruta.ciudad_origen.nombre);
                    }


                }

                /*else                
                {//throw new Exception("Filas no encontradas");
                }*/
                entidad.lstRen = lst;
                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }

        }


        public rendimiento obtener(rendimiento entidad, ConexionGenerica con)
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_rendimiento_listar";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_rendimiento"; param.DbType = DbType.Int32;
                param.Value = entidad.id_rendimiento; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_usuario"; param.DbType = DbType.Int32;
                param.Value = entidad.interfaz.usuario.id_usuario; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_registro"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_registro; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_inicial"; param.DbType = DbType.String;
                param.Value = ""; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_final"; param.DbType = DbType.String;
                param.Value = ""; objetoCommand.Parameters.Add(param);

                //---------------------------------------------------------------------------------------
                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                objetoDataAdapter.SelectCommand = objetoCommand;
                objetoDataSet = new DataSet();
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];


                if (num_rows > 0)
                {
                    num_rows = dt.Rows.Count;
                    for (int i = 0; i <= num_rows - 1; i++)
                    {
                        rendimiento obj = new rendimiento();

                        obj.id_rendimiento = (Int32)dt.Rows[i]["o_id_rendimiento"];
                        obj.ruta.id_ruta = (Int32)dt.Rows[i]["o_id_ruta"];
                        obj.ruta.ciudad_origen.nombre = (String)dt.Rows[i]["o_ruta_origen"];
                        obj.ruta.ciudad_destino.nombre = (String)dt.Rows[i]["o_ruta_destino"];
                        obj.ruta.nombre = obj.ruta.ciudad_origen.nombre + "-" + obj.ruta.ciudad_destino.nombre;
                        obj.peso_minimo = (Decimal)dt.Rows[i]["o_peso_minimo"];
                        obj.peso_maximo = (Decimal)dt.Rows[i]["o_peso_maximo"];
                        obj.rend = (Decimal)dt.Rows[i]["o_rend"];
                        obj.interfaz.usuario.id_usuario = (Int32)dt.Rows[i]["o_id_usuario"];
                        obj.interfaz.usuario.nombre = (String)dt.Rows[i]["o_usuario"];
                        obj.interfaz.fecha_registro = Convert.ToDateTime(dt.Rows[i]["o_fecha_registro"]);
                        obj.interfaz.estado = (Boolean)dt.Rows[i]["o_estado"];

                        if (i == 0) entidad = obj;
                        entidad.lstRen.Add(obj);

                    }

                }



                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }

        }


    }
}

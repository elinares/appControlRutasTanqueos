﻿using appControlRutasTanqueos.Repositorio.Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appControlRutasTanqueos.Repositorio.Impl
{
    public class RepositorioBase<TEntidad,TContext> : IRepositorio<TEntidad>
        where TEntidad: entidad
        where TContext: DbContext
    {
        protected TContext context;
        public RepositorioBase(TContext dbContext)
        {
           context = dbContext;
        }

        public virtual TEntidad generar_iae(TEntidad entidad)
        {

            return entidad;
        }
        public virtual TEntidad listar(TEntidad entidad)
        {
            return entidad;
        }
        public virtual TEntidad obtener(TEntidad entidad)
        {
            return entidad;
        }

    }
}

﻿using appControlRutasTanqueos.Repositorio.Dominio;
using appControlRutasTanqueos.Repositorio.EF;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;

namespace appControlRutasTanqueos.Repositorio.Impl
{
    public class RepositorioTanqueo : RepositorioBase<tanqueo, VOfficeDbContext>
    {     
        public List<tanqueo> lst = null;
        private String name_sp;
        private DbCommand objetoCommand = null;
        private DbDataAdapter objetoDataAdapter = null;
        private DataSet objetoDataSet = null;
        private DataTable dt = null;

        public RepositorioTanqueo(VOfficeDbContext context)
            : base(context)
        {
            dt = new DataTable();
            lst = new List<tanqueo>();
        }


        public tanqueo generar_iae(tanqueo entidad, ConexionGenerica con)
        {
            
            try
            {
            
                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_tanqueo_iae";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter(); param.Direction = ParameterDirection.Output;
                param.ParameterName = "o_id_tanqueo"; param.DbType = DbType.Int32;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_combustible"; param.DbType = DbType.Int32;
                param.Value = entidad.id_tanqueo; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_recorrido_detalle"; param.DbType = DbType.Int32;
                param.Value = entidad.id_recorrido_detalle;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_prog"; param.DbType = DbType.Int32;
                param.Value = entidad.id_programacion; objetoCommand.Parameters.Add(param); 
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_unid_transp"; param.DbType = DbType.Int32;
                param.Value = entidad.unidad_transporte.id_unidad_transporte; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_empresa"; param.DbType = DbType.Int32;
                param.Value = entidad.empresa.id_persona; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_serie_comprobante"; param.DbType = DbType.String;
                param.Value = entidad.serie; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nro_comprobante"; param.DbType = DbType.String;
                param.Value = entidad.numero; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nro_galones"; param.DbType = DbType.Decimal;
                param.Value = entidad.nro_galones_abast;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nro_galones_ant"; param.DbType = DbType.Decimal;
                param.Value = entidad.nro_galones_anterior; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_kilometraje"; param.DbType = DbType.Decimal;
                param.Value = entidad.kilometraje;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_kilometraje_ant"; param.DbType = DbType.Decimal;
                param.Value = entidad.kilometraje_anterior; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_tanqueo"; param.DbType = DbType.String;
                param.Value = Convert.ToString(entidad.fecha).Substring(0, 10) +" "+ entidad.hora+":"+entidad.minuto;
                objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_precio"; param.DbType = DbType.Decimal;
                param.Value = entidad.precio; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_glosa"; param.DbType = DbType.String;//null
                param.Value = entidad.glosa; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_usuario"; param.DbType = DbType.Int32;
                param.Value = entidad.interfaz.usuario.id_usuario;objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_combustible"; param.DbType = DbType.String;
                param.Value = entidad.tipo_combustible.nombre; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_tanqueo"; param.DbType = DbType.String;
                param.Value = entidad.tipo_tanqueo; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_tcg"; param.DbType = DbType.Int32;
                param.Value = 0; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_total"; param.DbType = DbType.Decimal;
                param.Value = entidad.total; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_ind_nodesemb"; param.DbType = DbType.Boolean;
                param.Value = entidad.ind_nodesemb; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_detalle_ruta"; param.DbType = DbType.String;
                param.Value = entidad.detalle_ruta; objetoCommand.Parameters.Add(param);//null
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta_retorno"; param.DbType = DbType.Int32;//obtener
                param.Value = entidad.ruta_retorno.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_ind_ficticio"; param.DbType = DbType.Boolean;
                param.Value = entidad.ind_ficticio; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();

                objetoCommand.ExecuteNonQuery();
                entidad.id_tanqueo = (Int32)objetoCommand.Parameters["o_id_tanqueo"].Value;


            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {
            }

            return entidad;
        }


        public tanqueo listar(tanqueo entidad, ConexionGenerica con)
        {
                
            try
            {
                
                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_tanqueo_listar";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_combustible"; param.DbType = DbType.Int32;
                param.Value = entidad.id_tanqueo; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_prog"; param.DbType = DbType.Int32;
                param.Value = entidad.id_programacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_unid_transp"; param.DbType = DbType.Int32;
                param.Value = entidad.unidad_transporte.id_unidad_transporte; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_empresa"; param.DbType = DbType.Int32;
                param.Value = entidad.empresa.id_persona; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_serie_comprobante"; param.DbType = DbType.String;
                param.Value = entidad.serie; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nro_comprobante"; param.DbType = DbType.String;
                param.Value = entidad.numero; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_tanqueo"; param.DbType = DbType.String;
                param.Value = entidad.fecha; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta_retorno"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta_retorno.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_ind_ficticio"; param.DbType = DbType.Boolean;
                param.Value = entidad.ind_ficticio; objetoCommand.Parameters.Add(param);
                                
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_ini"; param.DbType = DbType.String;
                param.Value = Convert.ToString(entidad.interfaz.fecha_inicial).Substring(0, 10);
                objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_fin"; param.DbType = DbType.String;
                param.Value = Convert.ToString(entidad.interfaz.fecha_final).Substring(0, 10);
                objetoCommand.Parameters.Add(param);


                // Obtenemos  el DataAdapter siempre despues de fijar la consulta en Command
                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                // Adaptador enlazado a la consulta
                objetoDataAdapter.SelectCommand = objetoCommand;
                // Instanciamos el DataSet
                objetoDataSet = new DataSet();
                // Llenamos el DataSet con los datos obtenidos de la consulta
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];

                if (num_rows > 0)
                {
                    //num_rows = ds.Tables[0].Rows.Count;
                    num_rows = dt.Rows.Count;
                    for (int i = 0; i <= num_rows - 1; i++)
                    {
                        tanqueo obj = new tanqueo();

                        obj.id_tanqueo = (Int32)dt.Rows[i]["o_id_combustible"];
                        obj.id_programacion = (Int32)dt.Rows[i]["o_id_prog"];
                        obj.unidad_transporte.id_unidad_transporte = (Int32)dt.Rows[i]["o_id_unid_transp"];
                        obj.unidad_transporte.placa = (String)dt.Rows[i]["o_placa"];
                        obj.unidad_transporte.conductor.id_persona = (Int32)dt.Rows[i]["o_id_conductor"];
                        obj.unidad_transporte.conductor.nombres_apellidos = (String)dt.Rows[i]["o_conductor"];
                        obj.empresa.id_persona = (Int32)dt.Rows[i]["o_id_empresa"];
                        obj.empresa.nombres_apellidos = (String)dt.Rows[i]["o_empresa"];
                        obj.proveedor.id_persona = (Int32)dt.Rows[i]["o_id_sucursal"];
                        obj.proveedor.nombres_apellidos = (String)dt.Rows[i]["o_sucursal"];                        
                        obj.serie = (String)dt.Rows[i]["o_serie_comprobante"];
                        obj.numero = (String)dt.Rows[i]["o_nro_comprobante"];
                        obj.serie_numero = obj.serie + "-" + obj.numero;
                        obj.nro_galones_abast = (Decimal)dt.Rows[i]["o_nro_galones"];
                        obj.nro_galones_anterior = (Decimal)dt.Rows[i]["o_nro_galones_ant"];
                        obj.kilometraje = (Decimal)dt.Rows[i]["o_kilometraje"];
                        obj.kilometraje_anterior = (Decimal)dt.Rows[i]["o_kilometraje_ant"];
                        obj.fecha = Convert.ToDateTime(dt.Rows[i]["o_fecha_tanqueo"]);
                        obj.hora = (String)dt.Rows[i]["o_hora"];
                        obj.minuto= (String)dt.Rows[i]["o_minuto"];
                        

                        obj.precio = (Decimal)dt.Rows[i]["o_precio"];
                        obj.total = (Decimal)dt.Rows[i]["o_total"];
                        obj.glosa = (String)dt.Rows[i]["o_glosa"];
                        obj.interfaz.estado = (Boolean)dt.Rows[i]["o_estado"];
                        obj.interfaz.usuario.id_usuario = (Int32)dt.Rows[i]["o_id_usuario"];
                        obj.tipo_tanqueo = (String)dt.Rows[i]["o_tipo_tanqueo"];
                        obj.tipo_combustible.nombre = (String)dt.Rows[i]["o_tipo_combustible"];                        
                        obj.ind_nodesemb = (Boolean)dt.Rows[i]["o_ind_nodesemb"];
                        obj.interfaz.fecha_registro = Convert.ToDateTime(dt.Rows[i]["o_fecha_registro"]);
                        obj.detalle_ruta = (String)dt.Rows[i]["o_detalle_ruta"];
                        obj.ruta.id_ruta = (Int32)dt.Rows[i]["o_id_ruta"];
                        obj.ruta.nombre = (String)dt.Rows[i]["o_ruta"];
                        obj.ruta_retorno.id_ruta = (Int32)dt.Rows[i]["o_id_ruta_retorno"];
                        obj.ruta_retorno.nombre = (String)dt.Rows[i]["o_ruta_retorno"];
                        obj.ind_ficticio = (Boolean)dt.Rows[i]["o_ind_ficticio"];
                        /*
                        obj.ruta.ciudad_origen.nombre = (String)dt.Rows[i]["o_ruta_origen"];
                        obj.ruta.ciudad_destino.nombre = (String)dt.Rows[i]["o_ruta_destino"];
                        obj.ruta.nombre = obj.ruta.ciudad_origen.nombre + "-" + obj.ruta.ciudad_destino.nombre;
                        */
                        lst.Add(obj);
                        //Console.WriteLine(obj.ruta.ciudad_origen.nombre);
                    }


                }

                /*else                
                {//throw new Exception("Filas no encontradas");
                }*/
                entidad.lstTan = lst;
                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }

        }


        public tanqueo obtener(tanqueo entidad, ConexionGenerica con)
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_tanqueo_listar";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_combustible"; param.DbType = DbType.Int32;
                param.Value = entidad.id_tanqueo; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_prog"; param.DbType = DbType.Int32;
                param.Value = entidad.id_programacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_unid_transp"; param.DbType = DbType.Int32;
                param.Value = entidad.unidad_transporte.id_unidad_transporte; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_empresa"; param.DbType = DbType.Int32;
                param.Value = entidad.empresa.id_persona; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_serie_comprobante"; param.DbType = DbType.String;
                param.Value = entidad.serie; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nro_comprobante"; param.DbType = DbType.String;
                param.Value = entidad.numero; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_tanqueo"; param.DbType = DbType.String;
                param.Value = entidad.fecha; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta_retorno"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta_retorno.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_ind_ficticio"; param.DbType = DbType.Boolean;
                param.Value = entidad.ind_ficticio; objetoCommand.Parameters.Add(param);
                
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_inicial"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_inicial;
                objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_final"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_final;
                objetoCommand.Parameters.Add(param);

                //---------------------------------------------------------------------------------------
                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                objetoDataAdapter.SelectCommand = objetoCommand;
                objetoDataSet = new DataSet();
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];


                if (num_rows > 0)
                {
                    num_rows = dt.Rows.Count;
                    for (int i = 0; i <= num_rows - 1; i++)
                    {

                        tanqueo obj = new tanqueo();

                        obj.id_tanqueo = (Int32)dt.Rows[i]["o_id_combustible"];
                        obj.id_programacion = (Int32)dt.Rows[i]["o_id_prog"];
                        obj.unidad_transporte.id_unidad_transporte = (Int32)dt.Rows[i]["o_id_unid_transp"];
                        obj.unidad_transporte.placa = (String)dt.Rows[i]["o_placa"];
                        obj.unidad_transporte.conductor.id_persona = (Int32)dt.Rows[i]["o_id_conductor"];
                        obj.unidad_transporte.conductor.nombres_apellidos = (String)dt.Rows[i]["o_conductor"];
                        obj.empresa.id_persona = (Int32)dt.Rows[i]["o_id_empresa"];
                        obj.empresa.nombres_apellidos = (String)dt.Rows[i]["o_empresa"];
                        obj.proveedor.id_persona = (Int32)dt.Rows[i]["o_id_sucursal"];
                        obj.proveedor.nombres_apellidos = (String)dt.Rows[i]["o_sucursal"];
                        obj.serie = (String)dt.Rows[i]["o_serie_comprobante"];
                        obj.numero = (String)dt.Rows[i]["o_nro_comprobante"];
                        obj.serie_numero = obj.serie + "-" + obj.numero;                                                
                        obj.nro_galones_abast = (Decimal)dt.Rows[i]["o_nro_galones"];
                        obj.nro_galones_anterior = (Decimal)dt.Rows[i]["o_nro_galones_ant"];
                        obj.kilometraje = (Decimal)dt.Rows[i]["o_kilometraje"];
                        obj.kilometraje_anterior = (Decimal)dt.Rows[i]["o_kilometraje_ant"];

                        obj.fecha = Convert.ToDateTime(dt.Rows[i]["o_fecha_tanqueo"]);
                        obj.hora = (String)dt.Rows[i]["o_hora"];
                        obj.minuto = (String)dt.Rows[i]["o_minuto"];

                        obj.precio = (Decimal)dt.Rows[i]["o_precio"];
                        obj.glosa = (String)dt.Rows[i]["o_glosa"];
                        obj.interfaz.estado = (Boolean)dt.Rows[i]["o_estado"];
                        obj.interfaz.usuario.id_usuario = (Int32)dt.Rows[i]["o_id_usuario"];
                        obj.tipo_tanqueo = (String)dt.Rows[i]["o_tipo_tanqueo"];
                        obj.tipo_combustible.nombre = (String)dt.Rows[i]["o_tipo_combustible"];
                        obj.total = (Decimal)dt.Rows[i]["o_total"];
                        obj.ind_nodesemb = (Boolean)dt.Rows[i]["o_ind_nodesemb"];
                        obj.interfaz.fecha_registro = Convert.ToDateTime(dt.Rows[i]["o_fecha_registro"]);
                        obj.detalle_ruta = (String)dt.Rows[i]["o_detalle_ruta"];
                        obj.ruta.id_ruta = (Int32)dt.Rows[i]["o_id_ruta"];
                        obj.ruta.nombre = (String)dt.Rows[i]["o_ruta"];
                        obj.ruta_retorno.id_ruta = (Int32)dt.Rows[i]["o_id_ruta_retorno"];
                        obj.ruta_retorno.nombre = (String)dt.Rows[i]["o_ruta_retorno"];
                        obj.ind_ficticio = (Boolean)dt.Rows[i]["o_ind_ficticio"];

                        if (i == 0) entidad = obj;
                        entidad.lstTan.Add(obj);

                    }

                }



                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }

        }


    }
}

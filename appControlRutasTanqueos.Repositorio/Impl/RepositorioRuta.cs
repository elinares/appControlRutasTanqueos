﻿
using appControlRutasTanqueos.Repositorio.Dominio;
using appControlRutasTanqueos.Repositorio.EF;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Impl
{
     public class RepositorioRuta : RepositorioBase<ruta, VOfficeDbContext>

    {
        public List<ruta> lst = null;
        public List<persona> lstPer = null;
        private String name_sp;
        private DbCommand objetoCommand = null;
        private DbDataAdapter objetoDataAdapter = null;
        private DataSet objetoDataSet = null;
        private DataTable dt = null;


        public RepositorioRuta(VOfficeDbContext context)
            : base(context)
        {
            dt = new DataTable();
            lst = new List<ruta>();
            lstPer = new List<persona>();
        }




        public ruta listar(ruta entidad,  ConexionGenerica con)
        {            

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_ruta_listar";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.id_ruta;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ciudad_origen"; param.DbType = DbType.Int32;
                param.Value = entidad.ciudad_origen.id_distrito;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ciudad_destino"; param.DbType = DbType.Int32;
                param.Value = entidad.ciudad_destino.id_distrito;
                objetoCommand.Parameters.Add(param);param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nombre"; param.DbType = DbType.String;
                param.Value = entidad.nombre;
                objetoCommand.Parameters.Add(param);

                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_peso"; param.DbType = DbType.Decimal;
                param.Value = entidad.peso; objetoCommand.Parameters.Add(param); 

                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_usuario"; param.DbType = DbType.Int32;
                param.Value = entidad.interfaz.usuario.id_usuario;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_registro"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_registro;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_inicial"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_inicial;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_final"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_final;
                objetoCommand.Parameters.Add(param);

                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                objetoDataAdapter.SelectCommand = objetoCommand;
                objetoDataSet = new DataSet();
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];
                
                if (num_rows > 0)
                {
                    num_rows = dt.Rows.Count;
                    for (int i = 0; i <= num_rows - 1; i++)
                    {
                        ruta obj = new ruta();
                        obj.id_ruta = (Int32)dt.Rows[i]["o_id_ruta"];
                        obj.ciudad_origen.id_distrito = (Int32)dt.Rows[i]["o_id_ruta"];
                        obj.ciudad_origen.nombre = (String)dt.Rows[i]["o_ciudad_origen"];
                        obj.ciudad_destino.id_distrito = (Int32)dt.Rows[i]["o_id_ciudad_destino"];
                        obj.ciudad_destino.nombre = (String)dt.Rows[i]["o_ciudad_destino"];
                        obj.nombre = obj.ciudad_origen.nombre + "-" + obj.ciudad_destino.nombre;
                        obj.km_aprox = (Decimal)dt.Rows[i]["o_km_aprox"];
                        obj.tiempo_aprox = (Decimal)dt.Rows[i]["o_tiempo_aprox"];
                        obj.rend_esperado = (Decimal)dt.Rows[i]["o_rend_esperado"];

                        obj.interfaz.usuario.id_usuario = (Int32)dt.Rows[i]["o_id_usuario"];
                        obj.interfaz.usuario.nombre = (String)dt.Rows[i]["o_usuario"];
                        obj.interfaz.estado = (Boolean)dt.Rows[i]["o_estado"];
                        lst.Add(obj);
                    }


                }

                /*else                
                {//throw new Exception("Filas no encontradas");
                }*/
                entidad.lstRut = lst;
                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }

        }


        public ruta obtener(ruta entidad,  ConexionGenerica con )
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_ruta_listar";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.id_ruta;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ciudad_origen"; param.DbType = DbType.Int32;
                param.Value = entidad.ciudad_origen.id_distrito;
                objetoCommand.Parameters.Add(param); param = 
                
                objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ciudad_destino"; param.DbType = DbType.Int32;
                param.Value = entidad.ciudad_destino.id_distrito;
                objetoCommand.Parameters.Add(param);
                
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_peso"; param.DbType = DbType.Decimal;
                param.Value = entidad.peso;objetoCommand.Parameters.Add(param); 

                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_usuario"; param.DbType = DbType.Int32;
                param.Value = entidad.interfaz.usuario.id_usuario;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_registro"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_registro;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_inicial"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_inicial;
                objetoCommand.Parameters.Add(param); param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_final"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_final;
                objetoCommand.Parameters.Add(param);

                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                objetoDataAdapter.SelectCommand = objetoCommand;
                objetoDataSet = new DataSet();
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];
               

                if (num_rows > 0)
                {
                    entidad.id_ruta = (Int32)dt.Rows[0]["o_id_ruta"];
                    entidad.ciudad_origen.id_distrito = (Int32)dt.Rows[0]["o_id_ruta"];
                    entidad.ciudad_origen.nombre = (String)dt.Rows[0]["o_ciudad_origen"];
                    entidad.ciudad_destino.id_distrito = (Int32)dt.Rows[0]["o_id_ciudad_destino"];
                    entidad.ciudad_destino.nombre = (String)dt.Rows[0]["o_ciudad_destino"];
                    entidad.nombre = entidad.ciudad_origen.nombre + "-" + entidad.ciudad_destino.nombre;
                    entidad.km_aprox = (Decimal)dt.Rows[0]["o_km_aprox"];
                    entidad.tiempo_aprox = (Decimal)dt.Rows[0]["o_tiempo_aprox"];
                    entidad.rend_esperado = (Decimal)dt.Rows[0]["o_rend_esperado"];

                    entidad.interfaz.usuario.id_usuario = (Int32)dt.Rows[0]["o_id_usuario"];
                    entidad.interfaz.usuario.nombre = (String)dt.Rows[0]["o_usuario"];
                    //entidad.interfaz.fecha_registro = Convert.ToDateTime(dt.Rows[i]["o_fecha_registro"]);
                    entidad.interfaz.estado = (Boolean)dt.Rows[0]["o_estado"];

                }

                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }

        }





        public persona listar_persona(persona entidad, ConexionGenerica con)
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_persona_listar";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; 
                objetoCommand.Parameters.Add(param);
                
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_sucursal"; param.DbType = DbType.Int32;
                param.Value = entidad.id_persona;                
                objetoCommand.Parameters.Add(param); 

                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nombre"; param.DbType = DbType.String;
                param.Value = entidad.nombres_apellidos;
                objetoCommand.Parameters.Add(param); 
                
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado;
                objetoCommand.Parameters.Add(param); 

                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                objetoDataAdapter.SelectCommand = objetoCommand;
                objetoDataSet = new DataSet();
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];

                if (num_rows > 0)
                {
                    num_rows = dt.Rows.Count;
                    for (int i = 0; i <= num_rows - 1; i++)
                    {
                        persona obj = new persona();

                        obj.id_persona = (Int32)dt.Rows[i]["o_id_sucursal"];
                        obj.nombres_apellidos = (String)dt.Rows[i]["o_empresa_sucursal"];
                        obj.tarifa_combustible = (Decimal)dt.Rows[i]["o_tarifa_combustible"];
                        lstPer.Add(obj);

                        // Console.WriteLine(obj.ciudad_origen.nombre);
                    }


                }

                /*else                
                {//throw new Exception("Filas no encontradas");
                }*/
                entidad.lstPer = lstPer;
                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }

        }

    }
}
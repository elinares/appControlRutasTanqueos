﻿
using appControlRutasTanqueos.Repositorio.Dominio;
using appControlRutasTanqueos.Repositorio.EF;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Impl
{
    public class RepositorioRecorrido : RepositorioBase<recorrido, VOfficeDbContext>
    {
        //public List<recorridoDetalle> lst = null;
        private String name_sp;
        private DbCommand objetoCommand = null;
        private DbDataAdapter objetoDataAdapter = null;
        private DataSet objetoDataSet = null;
        private DataTable dt = null;

        public RepositorioRecorrido(VOfficeDbContext context)
            : base(context)
        {
            dt = new DataTable();
            //lst = new List<recorridoDetalle>();
        }


        public recorrido generar_iae(recorrido entidad, ConexionGenerica con)
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_recorrido_iae";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter(); param.Direction = ParameterDirection.Output;
                param.ParameterName = "o_id_recorrido"; param.DbType = DbType.Int32; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_recorrido"; param.DbType = DbType.Int32;
                param.Value = entidad.id_recorrido; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_programacion"; param.DbType = DbType.Int32;
                param.Value = entidad.programacion.id_programacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_unidad_transporte"; param.DbType = DbType.Int32;
                param.Value = entidad.unidad_transporte.id_unidad_transporte; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_conductor"; param.DbType = DbType.Int32;
                param.Value = entidad.conductor.id_persona; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_cliente"; param.DbType = DbType.Int32;
                param.Value = entidad.cliente.id_persona; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha"; param.DbType = DbType.String;
                param.Value =  Convert.ToString(entidad.fecha).Substring(0,10); objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_peso_mercaderia"; param.DbType = DbType.Decimal;
                param.Value = entidad.mercaderia.peso; objetoCommand.Parameters.Add(param);

                param = objetoCommand.CreateParameter();                
                param.ParameterName = "i_rend"; param.DbType = DbType.Decimal;
                param.Value = entidad.tanqueo.rend_esperado; objetoCommand.Parameters.Add(param);

                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_usuario"; param.DbType = DbType.Int32;
                param.Value = entidad.interfaz.usuario.id_usuario; objetoCommand.Parameters.Add(param);

                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_registro"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_registro; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado; objetoCommand.Parameters.Add(param);

                objetoCommand.ExecuteNonQuery();
                entidad.id_recorrido = (Int32)objetoCommand.Parameters["o_id_recorrido"].Value;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {
            }

            return entidad;
        }



        public recorrido obtener(recorrido entidad, ConexionGenerica con)
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_recorrido_listar";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_recorrido"; param.DbType = DbType.Int32;
                param.Value = entidad.id_recorrido; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_programacion"; param.DbType = DbType.Int32;
                param.Value = entidad.programacion.id_programacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_unidad_transporte"; param.DbType = DbType.Int32;
                param.Value = entidad.unidad_transporte.id_unidad_transporte; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_conductor"; param.DbType = DbType.Int32;
                param.Value = entidad.conductor.id_persona; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_cliente"; param.DbType = DbType.Int32;
                param.Value = entidad.cliente.id_persona; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha"; param.DbType = DbType.String;
                param.Value = entidad.fecha; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_peso_mercaderia"; param.DbType = DbType.Decimal;
                param.Value = entidad.mercaderia.peso; objetoCommand.Parameters.Add(param);

                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_rend"; param.DbType = DbType.Decimal;
                param.Value = entidad.tanqueo.rend_esperado; objetoCommand.Parameters.Add(param);

                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_usuario"; param.DbType = DbType.Int32;
                param.Value = entidad.interfaz.usuario.id_usuario; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_registro"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_registro; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_ini"; param.DbType = DbType.String;                
                param.Value = Convert.ToString(entidad.interfaz.fecha_inicial).Substring(0, 10); 
                objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_fin"; param.DbType = DbType.String;
                param.Value = Convert.ToString(entidad.interfaz.fecha_final).Substring(0, 10); 
                objetoCommand.Parameters.Add(param);


                // Obtenemos  el DataAdapter siempre despues de fijar la consulta en Command
                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                // Adaptador enlazado a la consulta
                objetoDataAdapter.SelectCommand = objetoCommand;
                // Instanciamos el DataSet
                objetoDataSet = new DataSet();
                // Llenamos el DataSet con los datos obtenidos de la consulta
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];

                if (num_rows > 0)
                {
                    //num_rows = ds.Tables[0].Rows.Count;
                    num_rows = dt.Rows.Count;
                    for (int i = 0; i <= num_rows - 1; i++)
                    {
                        recorrido obj = new recorrido();

                        obj.id_recorrido = (Int32)dt.Rows[i]["o_id_recorrido"];
                        obj.programacion.id_programacion = (Int32)dt.Rows[i]["o_id_programacion"];
                        obj.unidad_transporte.id_unidad_transporte = (Int32)dt.Rows[i]["o_id_unidad_transporte"];
                        obj.unidad_transporte.placa = (String)dt.Rows[i]["o_placa"];
                        obj.conductor.id_persona = (Int32)dt.Rows[i]["o_id_conductor"];
                        obj.conductor.nombres_apellidos = (String)dt.Rows[i]["o_conductor"];
                        obj.cliente.id_persona = (Int32)dt.Rows[i]["o_id_cliente"];
                        obj.cliente.nombres_apellidos = (String)dt.Rows[i]["_cliente"];
                        obj.tanqueo.kilometraje_anterior = (Decimal)dt.Rows[i]["o_kilometraje_ant"];
                        obj.tanqueo.nro_galones_anterior = (Decimal)dt.Rows[i]["o_nro_galones_ant"];
                        obj.ruta.id_ruta = (Int32)dt.Rows[i]["o_id_ruta"];
                        obj.ruta.ciudad_origen.nombre = (String)dt.Rows[i]["o_ruta_origen"];
                        obj.ruta.ciudad_destino.nombre = (String)dt.Rows[i]["o_ruta_destino"];
                        obj.ruta.nombre = obj.ruta.ciudad_origen.nombre + "-" + obj.ruta.ciudad_destino.nombre;
                        obj.fecha = Convert.ToDateTime(dt.Rows[i]["o_fecha"]);

                        obj.mercaderia.nombre = (String)dt.Rows[i]["o_descripcion_mercaderia"];
                        obj.mercaderia.peso = (Decimal)dt.Rows[i]["o_peso_mercaderia"];

                        obj.interfaz.usuario.id_usuario = (Int32)dt.Rows[i]["o_id_usuario"];
                        obj.interfaz.usuario.nombre = (String)dt.Rows[i]["o_usuario"];
                        obj.interfaz.fecha_registro = Convert.ToDateTime(dt.Rows[i]["o_fecha_registro"]);
                        obj.interfaz.estado = (Boolean)dt.Rows[i]["o_estado"];
                        obj.tanqueo.rend_esperado = (Decimal)dt.Rows[i]["o_rendimiento"];

                        if (i == 0) entidad = obj;
                        entidad.lstR.Add(obj);
                        //Console.WriteLine(obj.ruta.ciudad_origen.nombre);
                    }


                }

                /*else                
                {//throw new Exception("Filas no encontradas");
                }*/
                //entidad.lstRD = lst;
                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }

        }



        public recorrido obtenerJson(recorrido entidad, ConexionGenerica con)
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_recorrido_listar";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_recorrido"; param.DbType = DbType.Int32;
                param.Value = entidad.id_recorrido; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_programacion"; param.DbType = DbType.Int32;
                param.Value = entidad.programacion.id_programacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_unidad_transporte"; param.DbType = DbType.Int32;
                param.Value = entidad.unidad_transporte.id_unidad_transporte; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_conductor"; param.DbType = DbType.Int32;
                param.Value = entidad.conductor.id_persona; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_cliente"; param.DbType = DbType.Int32;
                param.Value = entidad.cliente.id_persona; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.ruta.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha"; param.DbType = DbType.String;
                param.Value = entidad.fecha; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_peso_mercaderia"; param.DbType = DbType.Decimal;
                param.Value = entidad.mercaderia.peso; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_rend"; param.DbType = DbType.Decimal;
                param.Value = entidad.tanqueo.rend_esperado; objetoCommand.Parameters.Add(param);


                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_usuario"; param.DbType = DbType.Int32;
                param.Value = entidad.interfaz.usuario.id_usuario; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_registro"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_registro; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_ini"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_inicial; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_fin"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_final; objetoCommand.Parameters.Add(param);


                // Obtenemos  el DataAdapter siempre despues de fijar la consulta en Command
                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                // Adaptador enlazado a la consulta
                objetoDataAdapter.SelectCommand = objetoCommand;
                // Instanciamos el DataSet
                objetoDataSet = new DataSet();
                // Llenamos el DataSet con los datos obtenidos de la consulta
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];

                if (num_rows > 0)
                {
                    //num_rows = ds.Tables[0].Rows.Count;
                    num_rows = dt.Rows.Count;
                    for (int i = 0; i <= num_rows - 1; i++)
                    {
                        recorrido obj = new recorrido();

                        obj.id_recorrido = (Int32)dt.Rows[i]["o_id_recorrido"];
                        obj.programacion.id_programacion = (Int32)dt.Rows[i]["o_id_programacion"];
                        obj.unidad_transporte.id_unidad_transporte = (Int32)dt.Rows[i]["o_id_unidad_transporte"];
                        obj.unidad_transporte.placa = (String)dt.Rows[i]["o_placa"];
                        obj.conductor.id_persona = (Int32)dt.Rows[i]["o_id_conductor"];
                        obj.conductor.nombres_apellidos = (String)dt.Rows[i]["o_conductor"];
                        obj.cliente.id_persona = (Int32)dt.Rows[i]["o_id_cliente"];
                        obj.cliente.nombres_apellidos = (String)dt.Rows[i]["_cliente"];
                        obj.tanqueo.kilometraje_anterior = (Decimal)dt.Rows[i]["o_kilometraje_ant"];
                        obj.tanqueo.nro_galones_anterior = (Decimal)dt.Rows[i]["o_nro_galones_ant"];

                        obj.ruta.id_ruta = (Int32)dt.Rows[i]["o_id_ruta"];
                        obj.ruta.ciudad_origen.nombre = (String)dt.Rows[i]["o_ruta_origen"];
                        obj.ruta.ciudad_destino.nombre = (String)dt.Rows[i]["o_ruta_destino"];
                        obj.ruta.nombre = obj.ruta.ciudad_origen.nombre + "-" + obj.ruta.ciudad_destino.nombre;
                        obj.fecha = Convert.ToDateTime(dt.Rows[i]["o_fecha"]);
                        obj.fecha_cadena = (String)(dt.Rows[i]["o_fecha"]);

                        obj.mercaderia.nombre = (String)dt.Rows[i]["o_descripcion_mercaderia"];
                        obj.mercaderia.peso = (Decimal)dt.Rows[i]["o_peso_mercaderia"];

                        obj.interfaz.usuario.id_usuario = (Int32)dt.Rows[i]["o_id_usuario"];
                        obj.interfaz.usuario.nombre = (String)dt.Rows[i]["o_usuario"];
                        obj.interfaz.fecha_registro = Convert.ToDateTime(dt.Rows[i]["o_fecha_registro"]);
                        obj.interfaz.estado = (Boolean)dt.Rows[i]["o_estado"];
                        obj.tanqueo.rend_esperado = (Decimal)dt.Rows[i]["o_rendimiento"];

                        //if (i == 0) entidad = obj;
                        entidad.lstR.Add(obj);
                        //Console.WriteLine(obj.ruta.ciudad_origen.nombre);
                    }


                }

                /*else                
                {//throw new Exception("Filas no encontradas");
                }*/
                //entidad.lstRD = lst;
                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }

        }


        
       


    }
}
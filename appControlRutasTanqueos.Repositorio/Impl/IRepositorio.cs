﻿using appControlRutasTanqueos.Repositorio.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appControlRutasTanqueos.Repositorio.Impl
{
    public interface IRepositorio<TEntidad> where TEntidad : entidad
    {
        TEntidad generar_iae(TEntidad entidad);
        TEntidad listar(TEntidad entidad);
        TEntidad obtener(TEntidad entidad);
        
    }
}

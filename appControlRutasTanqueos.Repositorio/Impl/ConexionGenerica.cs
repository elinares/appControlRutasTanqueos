﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appControlRutasTanqueos.Repositorio.Impl
{
    public class ConexionGenerica
    {
        /*
         * Fuente de como crear una conexion genérica para cualquier base de datos
         * http://joaquin.medina.name/web2008/documentos/informatica/lenguajes/puntoNET/basesDatos/ADO/2009_02_08_Acceso_Generico_A_Datos.html

         * 
Instalar el proveedor de datos .Net PostgreSQL

FUENTE : https://support.office.com/es-es/article/Conectarse-a-una-base-de-datos-PostgreSQL-consulta-de-energ%C3%ADa-bf941e52-066f-4911-a41f-2493c39e69e4?ui=es-ES&rs=es-ES&ad=ES

1. Utilizar GACUtil.exe para agregar Npgsql.dll y Mono.Security.dll en la caché global.
   
   1.1.Registrar las DLL ingresando a la consola CMD como Administrador.
   1.2.Accedemos al directorio C:\Program Files (x86)\Microsoft SDKs\Windows\v8.1A\bin\NETFX 4.5.1 Tools
       donde se encuentra la herramienta gacutil.exe 
   
   "C:\Program Files (x86)\Microsoft SDKs\Windows\v8.1A\bin\NETFX 4.5.1 Tools\gacutil.exe"  -i  "c:\temp\npgsql.dll"
   
   "C:\Program Files (x86)\Microsoft SDKs\Windows\v8.1A\bin\NETFX 4.5.1 Tools\gacutil.exe"  -i  "c:\temp\mono.security.dll"


2. CONFIGURAR el provider en el archivo machine.config de la ruta  C:\Windows\Microsoft.NET\Framework\v4.0.30319\Config.
   Tener en cuenta la version instalada del proveedor Postgres.

  <DbProviderFactories>
            

  <add name="Microsoft SQL Server Compact Data Provider 4.0" invariant="System.Data.SqlServerCe.4.0" description=".NET Framework Data Provider for Microsoft SQL Server Compact" type="System.Data.SqlServerCe.SqlCeProviderFactory, System.Data.SqlServerCe, Version=4.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"/>

  <add name="Npgsql Data Provider" 
       invariant="Npgsql" 
       description=".Net Framework Data Provider for PostgreSQL" 
       type="Npgsql.NpgsqlFactory, Npgsql, Version=3.0.2.0, Culture=neutral, PublicKeyToken=5d8b90d52f46fda7"/>


  </DbProviderFactories>

         
        */
        private String nombreInvarianteProveedor;
        private String cadenaDeConexion;
        public DbProviderFactory objetoDbFactoria;
        private DbConnection objetoDbConexion = null;
        private DbConnectionStringBuilder objetoConnectionStringBuilder = null;

        public ConexionGenerica() {


            try
            {
                nombreInvarianteProveedor = "Npgsql";
                cadenaDeConexion = "Server=192.168.1.5;Port=5432;User Id=postgres;Password=123456;Database=tvn;Pooling=false;";
                //cadenaDeConexion = "Server=localhost;Port=5432;User Id=postgres;Password=123456;Database=tvn_proyecto;Pooling=false;"; //timeout=1000;
                objetoDbFactoria = DbProviderFactories.GetFactory(nombreInvarianteProveedor);
                objetoConnectionStringBuilder = objetoDbFactoria.CreateConnectionStringBuilder();
                objetoConnectionStringBuilder.ConnectionString = cadenaDeConexion;
                //objetoConnectionStringBuilder.
                
                objetoDbConexion = objetoDbFactoria.CreateConnection();
                objetoDbConexion.ConnectionString = objetoConnectionStringBuilder.ConnectionString;                
                objetoDbConexion.Open();
                
                //if (objetoDbConexion.State != ConnectionState.Open) 
                
                //objetoDbConexion.Open();                
            }
            catch (Exception e)
            {
                if (objetoDbConexion.State == ConnectionState.Open)  
                     objetoDbConexion.Close();
                throw new Exception(e.Message);
            }


        }


        public DbProviderFactory obtenerFactoria()
        {

            try
            {
                return objetoDbFactoria;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        public DbConnection obtenerConexion()
        {

            try
            {
                return objetoDbConexion;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }


    }
}

﻿
using appControlRutasTanqueos.Repositorio.Dominio;
using appControlRutasTanqueos.Repositorio.EF;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Impl
{
    public class RepositorioRecorridoDetalle : RepositorioBase<recorridoDetalle, VOfficeDbContext>
    {
        //public List<recorridoDetalle> lst = null;
        private String name_sp;
        private DbCommand objetoCommand = null;
        private DbDataAdapter objetoDataAdapter = null;
        private DataSet objetoDataSet = null;
        private DataTable dt = null;

        public RepositorioRecorridoDetalle(VOfficeDbContext context)
            : base(context)
        {
            dt = new DataTable();
            //lst = new List<recorridoDetalle>();
        }


        public recorridoDetalle generar_iae(recorridoDetalle entidad, ConexionGenerica con)
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_recorrido_detalle_iae";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter(); param.Direction = ParameterDirection.Output;
                param.ParameterName = "o_id_recorrido_detalle"; param.DbType = DbType.Int32; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_recorrido_detalle"; param.DbType = DbType.Int32;
                param.Value = entidad.id_recorrido_detalle; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_recorrido"; param.DbType = DbType.Int32;
                param.Value = entidad.recorrido.id_recorrido; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.trayectoria.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_km_recor_esp"; param.DbType = DbType.Decimal;
                param.Value = entidad.km_recor_esp; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_km_recor_real"; param.DbType = DbType.Decimal;
                param.Value = entidad.km_recor_real; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nro_galones_abast"; param.DbType = DbType.Decimal;
                param.Value = entidad.nro_galones_abast; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nro_galones_asig"; param.DbType = DbType.Decimal;
                param.Value = entidad.nro_galones_asig; objetoCommand.Parameters.Add(param);

                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_rend_esp"; param.DbType = DbType.Decimal;
                param.Value = entidad.tanqueo.rend_esperado; objetoCommand.Parameters.Add(param);

                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_usuario"; param.DbType = DbType.Int32;
                param.Value = entidad.interfaz.usuario.id_usuario; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_registro"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_registro; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado; objetoCommand.Parameters.Add(param);
                

                objetoCommand.ExecuteNonQuery();
                entidad.id_recorrido_detalle = (Int32)objetoCommand.Parameters["o_id_recorrido_detalle"].Value;


            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {
            }

            return entidad;
        }
        public recorridoDetalle obtener(recorridoDetalle entidad, ConexionGenerica con)
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "prog_recorrido_detalle_listar";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter(); 
                param.ParameterName = "i_tipo_operacion"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.tipo_operacion; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_recorrido_detalle"; param.DbType = DbType.Int32;
                param.Value = entidad.id_recorrido_detalle; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_recorrido"; param.DbType = DbType.Int32;
                param.Value = entidad.recorrido.id_recorrido; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_id_ruta"; param.DbType = DbType.Int32;
                param.Value = entidad.trayectoria.id_ruta; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_km_recor_esp"; param.DbType = DbType.Decimal;
                param.Value = entidad.km_recor_esp; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_km_recor_real"; param.DbType = DbType.Decimal;
                param.Value = entidad.km_recor_real; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nro_galones_abast"; param.DbType = DbType.Decimal;
                param.Value = entidad.nro_galones_abast; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_nro_galones_asig"; param.DbType = DbType.Decimal;
                param.Value = entidad.nro_galones_asig; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();

                param.ParameterName = "i_usuario"; param.DbType = DbType.Int32;
                param.Value = entidad.interfaz.usuario.id_usuario; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_registro"; param.DbType = DbType.String;
                param.Value = entidad.interfaz.fecha_registro; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_estado"; param.DbType = DbType.Boolean;
                param.Value = entidad.interfaz.estado; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_ini"; param.DbType = DbType.String;
                param.Value = entidad.recorrido.interfaz.fecha_inicial; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_fin"; param.DbType = DbType.String;
                param.Value = entidad.recorrido.interfaz.fecha_final; objetoCommand.Parameters.Add(param);


                // Obtenemos  el DataAdapter siempre despues de fijar la consulta en Command
                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                // Adaptador enlazado a la consulta
                objetoDataAdapter.SelectCommand = objetoCommand;
                // Instanciamos el DataSet
                objetoDataSet = new DataSet();
                // Llenamos el DataSet con los datos obtenidos de la consulta
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];

                if (num_rows > 0)
                {
                    //num_rows = ds.Tables[0].Rows.Count;
                    num_rows = dt.Rows.Count;
                    for (int i = 0; i <= num_rows - 1; i++)
                    {
                        recorridoDetalle obj = new recorridoDetalle();
                        recorridoDetalle obj_Ant = new recorridoDetalle();

                        obj.id_recorrido_detalle = (Int32)dt.Rows[i]["o_id_recorrido_detalle"];
                        obj.recorrido.id_recorrido = (Int32)dt.Rows[i]["o_id_recorrido"];
                        obj.trayectoria.id_ruta = (Int32)dt.Rows[i]["o_id_ruta"];
                        obj.trayectoria.ciudad_origen.nombre = (String)dt.Rows[i]["o_ruta_origen"];
                        obj.trayectoria.ciudad_destino.nombre = (String)dt.Rows[i]["o_ruta_destino"];
                        obj.trayectoria.nombre = obj.trayectoria.ciudad_origen.nombre + "-" + obj.trayectoria.ciudad_destino.nombre;

                        obj.km_recor_esp = (Decimal)dt.Rows[i]["o_km_recor_esp"];
                        obj.km_recor_real = (Decimal)dt.Rows[i]["o_km_recor_real"];
                        obj.nro_galones_abast = (Decimal)dt.Rows[i]["o_nro_galones_abast"];
                        obj.nro_galones_asig = (Decimal)dt.Rows[i]["o_nro_galones_asig"];
                        obj.tanqueo.rend_esperado = (Decimal)dt.Rows[i]["o_rend_esp"];
                        obj.tanqueo.precio = (Decimal)dt.Rows[i]["o_precio"];

                        obj.recorrido.programacion.id_programacion = (Int32)dt.Rows[i]["o_id_programacion"];
                        obj.recorrido.unidad_transporte.id_unidad_transporte = (Int32)dt.Rows[i]["o_id_unidad_transporte"];
                        obj.recorrido.unidad_transporte.placa = (String)dt.Rows[i]["o_placa"];
                        obj.recorrido.unidad_transporte.conductor.id_persona = (Int32)dt.Rows[i]["o_id_conductor"];
                        obj.recorrido.unidad_transporte.conductor.nombres_apellidos = (String)dt.Rows[i]["o_conductor"];
                        obj.recorrido.cliente.id_persona = (Int32)dt.Rows[i]["o_id_cliente"];
                        obj.recorrido.cliente.nombres_apellidos = (String)dt.Rows[i]["_cliente"];

                        obj.recorrido.ruta.id_ruta = (Int32)dt.Rows[i]["o_id_ruta_rec"];
                        obj.recorrido.ruta.ciudad_origen.nombre = (String)dt.Rows[i]["o_ruta_origen_rec"];
                        obj.recorrido.ruta.ciudad_destino.nombre = (String)dt.Rows[i]["o_ruta_destino_rec"];
                        obj.recorrido.ruta.nombre = obj.recorrido.ruta.ciudad_origen.nombre + "-" + obj.recorrido.ruta.ciudad_destino.nombre;
                        obj.recorrido.fecha = Convert.ToDateTime(dt.Rows[i]["o_fecha"]);
                        obj.recorrido.mercaderia.nombre = (String)dt.Rows[i]["o_descripcion_mercaderia"];
                        obj.recorrido.mercaderia.peso = (Decimal)dt.Rows[i]["o_peso_mercaderia"];
                        


                        obj.interfaz.usuario.id_usuario = (Int32)dt.Rows[i]["o_id_usuario"];
                        obj.interfaz.usuario.nombre = (String)dt.Rows[i]["o_usuario"];
                        obj.interfaz.fecha_registro = Convert.ToDateTime(dt.Rows[i]["o_fecha_registro"]);
                        obj.interfaz.estado = (Boolean)dt.Rows[i]["o_estado"];

                        /*--Calculo del consumo de combustible y costo por ruta cuando no hay tanqueos en alguna de ellas---*/
                        if (i > 0) obj_Ant = entidad.lstRD[i - 1];
                        if (obj.nro_galones_abast == 0) 
                        {
                         obj.nro_galones_consum = obj.nro_galones_asig;
                         if (i > 0 && obj.tanqueo.precio==0) obj.tanqueo.precio = obj_Ant.tanqueo.precio; 
                        }
                        else obj.nro_galones_consum = obj.nro_galones_abast;                                                

                        if (i > 0 && obj_Ant.nro_galones_abast ==  0) 
                            obj.nro_galones_consum = obj.nro_galones_consum - obj_Ant.nro_galones_consum;

                        obj.tanqueo.total = obj.nro_galones_consum * obj.tanqueo.precio;
                        if (obj.nro_galones_consum>0) obj.tanqueo.rend_real = obj.km_recor_real / obj.nro_galones_consum;
                        obj.dif_glns = obj.nro_galones_asig - obj.nro_galones_abast;
                        obj.dif_km = obj.km_recor_esp - obj.km_recor_real;
                        obj.dif_rend = obj.tanqueo.rend_real - obj.tanqueo.rend_esperado;
                        /*---------------------------------------------------------------------------------------------------*/

                        obj.interfaz.tipo_operacion = entidad.interfaz.tipo_operacion;
                        if (i == 0) entidad = obj;
                        entidad.lstRD.Add(obj);
                        //Console.WriteLine(obj.ruta.ciudad_origen.nombre);
                    }


                }

                /*else                
                {//throw new Exception("Filas no encontradas");
                }*/
                //entidad.lstRD = lst;
                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }

        }

        public recorridoDetalle listar_traza_recorrido_detalle(recorridoDetalle entidad, ConexionGenerica con)
        {

            try
            {

                objetoCommand = con.obtenerConexion().CreateCommand();
                name_sp = "sp_listar_traza_recorrido_detalle";
                objetoCommand.CommandType = CommandType.StoredProcedure;
                objetoCommand.CommandText = name_sp;             

                //----------------------Parametros de entrada------------------------------------------
                DbParameter param = objetoCommand.CreateParameter();
                param.ParameterName = "i_unidad_tr"; param.DbType = DbType.Int32;
                param.Value = entidad.recorrido.unidad_transporte.id_unidad_transporte; objetoCommand.Parameters.Add(param);                
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_ini"; param.DbType = DbType.String;
                param.Value = entidad.recorrido.interfaz.fecha_inicial; objetoCommand.Parameters.Add(param);
                param = objetoCommand.CreateParameter();
                param.ParameterName = "i_fecha_fin"; param.DbType = DbType.String;
                param.Value = entidad.recorrido.interfaz.fecha_final; objetoCommand.Parameters.Add(param);

                //---------------------------------------------------------------------------------------
                objetoDataAdapter = con.obtenerFactoria().CreateDataAdapter();
                objetoDataAdapter.SelectCommand = objetoCommand;
                objetoDataSet = new DataSet();
                int num_rows = objetoDataAdapter.Fill(objetoDataSet);
                dt = objetoDataSet.Tables[0];

                if (num_rows > 0)
                {
                    num_rows = dt.Rows.Count;
                    for (int i = 0; i <= num_rows - 1; i++)
                    {
                        recorridoDetalle obj = new recorridoDetalle();
                        recorridoDetalle obj_Ant = new recorridoDetalle();

                        obj.recorrido.programacion.id_programacion = (Int32)dt.Rows[i]["_id_programacion"];
                        obj.tanqueo.id_tanqueo = (Int32)dt.Rows[i]["_id_combustible"];
                        obj.recorrido.unidad_transporte.id_unidad_transporte = (Int32)dt.Rows[i]["_id_ut"];
                        obj.recorrido.unidad_transporte.placa = (String)dt.Rows[i]["_placa"];
                        obj.recorrido.unidad_transporte.modelo = (String)dt.Rows[i]["_modelo"];
                        obj.recorrido.unidad_transporte.conductor.nombres_apellidos = (String)dt.Rows[i]["_chofer"];

                        obj.tanqueo.nro_galones_abast = (Decimal)dt.Rows[i]["_nro_galones"];
                        obj.tanqueo.kilometraje = (Decimal)dt.Rows[i]["_kilometraje"];
                        obj.tanqueo.fecha = Convert.ToDateTime(dt.Rows[i]["_fecha_tanqueo"]);
                        obj.tanqueo.precio = (Decimal)dt.Rows[i]["_precio"];
                        obj.tanqueo.total = (Decimal)dt.Rows[i]["_total"];
                        obj.tanqueo.proveedor.nombres_apellidos = (String)dt.Rows[i]["_razon_social"];
                        obj.tanqueo.distrito.id_distrito = (Int32)dt.Rows[i]["_id_sucursal"];
                        obj.tanqueo.distrito.nombre = (String)dt.Rows[i]["_sucursal"];
                        obj.tanqueo.tipo_tanqueo = (String)dt.Rows[i]["_tipo_tanqueo"];
                        obj.tanqueo.tipo_combustible.nombre = (String)dt.Rows[i]["_tipo_combustible"];
                        obj.recorrido.ruta.id_ruta = (Int32)dt.Rows[i]["_id_ruta"];
                        obj.recorrido.ruta.ciudad_origen.nombre = (String)dt.Rows[i]["_ruta_viaje_origen"];
                        obj.recorrido.ruta.ciudad_destino.nombre = (String)dt.Rows[i]["_ruta_viaje_destino"];                                                    
                        obj.recorrido.ruta.nombre = obj.recorrido.ruta.ciudad_origen.nombre +"-" +obj.recorrido.ruta.ciudad_destino.nombre;
                        obj.recorrido.mercaderia.unidad_medida.id_unidad_medida = (Int32)dt.Rows[i]["_id_um"];
                        obj.recorrido.mercaderia.unidad_medida.abreviatura = (String)dt.Rows[i]["_um_abrev"];
                        obj.recorrido.mercaderia.peso = (Decimal)dt.Rows[i]["_cant"];
                        obj.recorrido.mercaderia.cliente.nombres_apellidos = (String)dt.Rows[i]["_cliente"];

                        obj.trayectoria.id_ruta = (Int32)dt.Rows[i]["_id_ruta1"];
                        obj.trayectoria.nombre = (String)dt.Rows[i]["_ruta_viaje1"];
                        obj.tanqueo.serie_numero = (String)dt.Rows[i]["_serie_numero_comprobante"];
                        obj.tanqueo.rend_esperado = (Decimal)dt.Rows[i]["o_rend_esperado"];

                        if (i > 0) obj_Ant = entidad.lstRD[i - 1];
                        if (i > 0 && obj_Ant.recorrido.unidad_transporte.placa == obj.recorrido.unidad_transporte.placa)
                        {
                            obj.tanqueo.kilometraje_anterior = obj_Ant.tanqueo.kilometraje;
                            if (obj.tanqueo.nro_galones_abast > 0)
                                obj.tanqueo.rend_real = (obj.tanqueo.kilometraje - obj_Ant.tanqueo.kilometraje) / obj.tanqueo.nro_galones_abast;
                            
                            if (obj.tanqueo.rend_esperado > 0)
                                obj.tanqueo.nro_galones_asig = (obj.tanqueo.kilometraje - obj_Ant.tanqueo.kilometraje) / obj.tanqueo.rend_esperado;

                        }

                        entidad.lstRD.Add(obj);
                    }


                }
                else
                {

                    //throw new Exception("Filas no encontradas");
                }

                //entidad.lstRD = lst;

                return entidad;

            }
            catch (Exception e)
            {
                throw (new Exception(e.Message));
            }
            finally
            {
                //con.Close();
            }



        }
       


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class claseDocumento : entidad
    {
        public int id_clase_documento { get; set; }
        public string nombre { get; set; }

        public claseDocumento()
        {
            this.id_clase_documento = 0;
        }
    }
}
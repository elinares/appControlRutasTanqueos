﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class unidadTransporte : entidad
    {
        public int id_unidad_transporte { get; set; }
        public string placa { get; set; }
        public string modelo { get; set; }
        public persona conductor { get; set; }

        public unidadTransporte()
        {
            this.id_unidad_transporte = 0;
            this.conductor = new persona();

        }
    }
}
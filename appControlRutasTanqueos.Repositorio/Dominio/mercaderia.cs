﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class mercaderia : entidad
    {
        public int id_mercaderia { get; set; }
        public string nombre { get; set; }
        public decimal peso { get; set; }
        public unidadMedida unidad_medida { get; set; }
        public persona cliente { set; get; }

        public mercaderia() {
            this.unidad_medida = new unidadMedida();
            this.cliente = new persona();
            this.peso = 0;
        }


    }
}
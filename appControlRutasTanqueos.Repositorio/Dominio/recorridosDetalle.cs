﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class recorridoDetalle : entidad
    {
        public int id_recorrido_detalle { get; set; }
        public recorrido recorrido { get; set; }
        public ruta trayectoria { get; set; } // Define los recorridos dentro de la ruta
        public Decimal km_recor_esp { get; set; }
        public Decimal km_recor_real { get; set; }
        public Decimal nro_galones_abast { get; set; }//Almacena el total de galones abastecidos en un recorrido con uno o mas tanqueos
        public Decimal nro_galones_asig { get; set; }//Almacena el total de galones asignados en un recorrido con uno o mas tanqueos
        public Decimal nro_galones_consum { get; set; }
        public Decimal dif_km { get; set; }
        public Decimal dif_glns { get; set; }
        public Decimal dif_rend { get; set; }
        public List<String> lstAct { get; set; }//Lista de actividades: Descarga mercaderia origen, Descarga mercaderia en destino, Tanqueo en origen, Tanqueo en destino, etc                
        public tanqueo tanqueo { get; set; }
        public interfaz interfaz { get; set; }
        public List<recorridoDetalle> lstRD { get; set; }


        public recorridoDetalle()
        {

            this.id_recorrido_detalle = 0;
            this.recorrido = new recorrido();
            this.trayectoria = new ruta();
            this.tanqueo = new tanqueo();
            this.interfaz = new interfaz();
            this.km_recor_esp = 0;
            this.km_recor_real = 0;
            this.nro_galones_abast = 0;
            this.nro_galones_asig = 0;
            this.lstAct = new List<String>();
            this.lstRD = new List<recorridoDetalle>();
           

        }



    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class interfaz
    {

        public String tipo_operacion { get; set; }
        public String titulo { get; set; }
        public String title { get; set; }
        public String subtitulo { get; set; }
        public String mensaje { get; set; }
        public usuario usuario { get; set; }
        public DateTime fecha_registro { get; set; }
        public Boolean estado { get; set; }
        public int contador { get; set; }

        public DateTime fecha_inicial { get; set; }
        public DateTime fecha_final { get; set; }

        public interfaz()
        {

            this.usuario = new usuario();
            this.fecha_final = DateTime.Now;
            this.fecha_inicial = DateTime.Now;
            this.fecha_registro = DateTime.Now;
            this.tipo_operacion = "1";
            this.contador = 0;
            this.estado = true;
            this.titulo = "Control de Rutas y Tanqueos";
            this.title = "CONTROL DE RUTAS Y TANQUEOS - TVN S.A.C.";
        }


    }
}
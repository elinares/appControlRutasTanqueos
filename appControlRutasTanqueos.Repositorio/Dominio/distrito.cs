﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class distrito : entidad
    {
        public int id_distrito { get; set; }
        public string nombre { get; set; }

        public distrito()
        {
            this.id_distrito = 0;            
        }

    }
}
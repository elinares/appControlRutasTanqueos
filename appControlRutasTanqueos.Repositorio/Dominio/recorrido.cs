﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class recorrido : entidad
    {
        public int id_recorrido { get; set; }
        public DateTime fecha { get; set; }
        public String fecha_cadena { get; set; }
        public unidadTransporte unidad_transporte { get; set; }
        public programacion programacion { get; set; }
        public tanqueo tanqueo { get; set; }
        public persona conductor { get; set; }
        public persona cliente { get; set; }
        public mercaderia mercaderia { get; set; }
        public ruta ruta { get; set; } // Define la ruta de origen y destino
        public interfaz interfaz { get; set; }

        public List<recorrido> lstR { get; set; }
        public List<recorridoDetalle> lstRD { get; set; }
        public List<ruta> lstRut { get; set; }
        public List<tanqueo> lstTan { get; set; } 
         
        public recorrido()
        {
            this.id_recorrido = 0;
            this.fecha= DateTime.Now;
            this.interfaz = new interfaz();
            this.unidad_transporte = new unidadTransporte();
            this.programacion = new programacion();
            this.conductor = new persona();
            this.cliente = new persona();
            this.mercaderia = new mercaderia();
            this.tanqueo = new tanqueo();
            this.ruta = new ruta();
            this.interfaz=new interfaz();
            this.lstR = new List<recorrido>();
            this.lstRD = new List<recorridoDetalle>();

        }

    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class programacion : entidad
    {
        public int id_programacion { get; set; }
        public usuario usuario { get; set; }
        public DateTime fecha { get; set; }

        public programacion()
        {
            this.id_programacion = 0;
            this.usuario = new usuario();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class unidadMedida : entidad
    {
        public int id_unidad_medida { get; set; }
        public string nombre { get; set; }
        public string abreviatura { get; set; }

        public unidadMedida()
        {

            this.id_unidad_medida = 0;

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class rendimiento : entidad
    {
        public int id_rendimiento { get; set; }
        public ruta ruta { get; set; }
        public decimal peso_minimo { get; set; }
        public decimal peso_maximo { get; set; }
        public decimal rend { get; set; } //Rendimiento
        public interfaz interfaz { get; set; }
        public List<rendimiento> lstRen { get; set; }

        public rendimiento()
        {
            this.id_rendimiento = 0;
            this.ruta = new ruta();
            this.peso_minimo = 0;
            this.peso_maximo = 0;
            this.rend = 0;
            this.interfaz = new interfaz();
            this.lstRen = new List<rendimiento>();
        }


    }

}

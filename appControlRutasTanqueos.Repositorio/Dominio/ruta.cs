﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appControlRutasTanqueos.Repositorio.Dominio
{
    public class ruta : entidad
    {
        public int id_ruta { get; set; }
        public String nombre { get; set; }        
        public distrito ciudad_origen { get; set; }
        public distrito ciudad_destino { get; set; }
        public decimal tiempo_aprox { get; set; }
        public decimal km_aprox { get; set; }
        public decimal peso { get; set; }
        public decimal rend_esperado { get; set; }
        public interfaz interfaz { get; set; }

        public List<ruta> lstRut { get; set; }        

        public ruta()
        {

            this.id_ruta = 0;
            this.nombre = "";
            this.tiempo_aprox = 0;
            this.km_aprox = 0;
            this.peso = 0;
            this.interfaz = new interfaz();
            this.ciudad_destino = new distrito();
            this.ciudad_origen = new distrito();
        }


    }
}